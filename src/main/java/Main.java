import block.BlockWithElevators;
import elevator.*;
import floor.Floor;
import person.ElevatorUser;
import utils.JsonUtil;
import weight.WeightMetrics;
import weight.Weigth;

import java.util.*;

/**
 * Aplikacja symuluje prace wind w budynku. W budynku znajdują się cztery windy
 * (trzy pasażerskie i jedna towarowa). Każda z wind pasażerskich obsługuje różną
 * ilość pięter (jedna z wind obsługuje wszystkie piętra w budynku, druga obsługuje
 * piętra pażyste a trzecia obsługuje piętra nieparzyste). Winda towarowa obsługuje
 * wszystkie piętra. Po uruchomienu aplikacji uruchomi się elevatorManager który
 * będzię nasłuchiwał czy nadeszły jakieś żądania od oczekujących użytkowników.
 * W konsoli należy wcisnąć ENTER aby nastąpiło wczytanie użytkowników z plików
 * textowych. Wszyscy użytkownicy, którzy spełniają odpowiednie kryteria (masa
 * użytkownika mniejsza od udźwigu windy oraz piętro na którym jest użytkownik i
 * piętro na które chce jechać użytkownik są w budynku) zostaną dodani do kolejek
 * na odpowiednich piętrach a ich żądania rozdysponowane pomiędzy dostępnymi windami.
 * ElevatorManager uruchomi windy, które zaczną rozwozić pasażerów.
 */
public class Main {

    public static void main(String[] args) {

        //listy pięter
        List<Floor> wszystkiePietra = new ArrayList<>(Arrays.asList(new Floor(-1),
                new Floor(0), new Floor(1), new Floor(2), new Floor(3),
                new Floor(4), new Floor(5), new Floor(6), new Floor(7),
                new Floor(8), new Floor(9), new Floor(10)));

        List<Floor> parzystePietra = new ArrayList<>(Arrays.asList(new Floor(0), new Floor(2),
                new Floor(4), new Floor(6), new Floor(8), new Floor(10)));

        List<Floor> nieParzystePietra = new ArrayList<>(Arrays.asList(new Floor(-1), new Floor(0),
                new Floor(1), new Floor(3), new Floor(5), new Floor(7), new Floor(9)));

        //tworze blok z windami
        BlockWithElevators blockWithElevators = new BlockWithElevators(wszystkiePietra);

        //tworze windy
        Elevator windaPasazerskaWszystkiePietra = new PassengerElevator();

        Elevator windaPasazerskaParzystePietra = new PassengerElevator();

        Elevator windaPasazerskaNieParzystePietra = new PassengerElevator();

        Elevator windaTowarowaWszystkiePietra = new ServiceElevator();

        //dodaje dostępne piętra do wind
        windaPasazerskaWszystkiePietra.setAvailableFloors(wszystkiePietra);

        windaPasazerskaParzystePietra.setAvailableFloors(parzystePietra);

        windaPasazerskaNieParzystePietra.setAvailableFloors(nieParzystePietra);

        windaTowarowaWszystkiePietra.setAvailableFloors(wszystkiePietra);

        //ustawiam windy na piętrze 0
        windaPasazerskaWszystkiePietra.setCurrentFloor(new Floor(0));

        windaPasazerskaParzystePietra.setCurrentFloor(new Floor(0));

        windaPasazerskaNieParzystePietra.setCurrentFloor(new Floor(0));

        windaTowarowaWszystkiePietra.setCurrentFloor(new Floor(0));

        //definiuje dopuszczalne obciażenie wind
        windaPasazerskaWszystkiePietra.setMaxLoad(new Weigth(400, WeightMetrics.KG));

        windaPasazerskaParzystePietra.setMaxLoad(new Weigth(400, WeightMetrics.KG));

        windaPasazerskaNieParzystePietra.setMaxLoad(new Weigth(400, WeightMetrics.KG));

        windaTowarowaWszystkiePietra.setMaxLoad(new Weigth(400, WeightMetrics.KG));


        //definiuje początkowy kierunek ruch wind
        windaPasazerskaWszystkiePietra.setDirection(Direction.NONE);

        windaPasazerskaParzystePietra.setDirection(Direction.NONE);

        windaPasazerskaNieParzystePietra.setDirection(Direction.NONE);

        windaTowarowaWszystkiePietra.setDirection(Direction.NONE);


        //dodaje windy do bloku
        LinkedList<Elevator> listaWindWBudynku = new LinkedList<>();

        listaWindWBudynku.add(windaPasazerskaWszystkiePietra);
        listaWindWBudynku.add(windaPasazerskaParzystePietra);
        listaWindWBudynku.add(windaPasazerskaNieParzystePietra);
        listaWindWBudynku.add(windaTowarowaWszystkiePietra);

        blockWithElevators.setElevators(listaWindWBudynku);


        //tworze obiekt managera który uruchomi metode startWork() w równoległym wątku
        ElevatorManager manager = new ElevatorManager(blockWithElevators);
        manager.startWork();

        //tworze obiekt JsonUtil który pobierze użytkowników z plików tekstowych
        JsonUtil jsonUtil = new JsonUtil();


        List<ElevatorUser> pasazerowie = jsonUtil.readJsons("Passenger", "src/main/resources/passengers.txt");
        List<ElevatorUser> towary = jsonUtil.readJsons("Cargo", "src/main/resources/cargoes.txt");

        Scanner scanner = new Scanner(System.in);
        System.out.println("Naciśnij ENTER aby dodać użytkowników");
        scanner.nextLine();

        //dodaje użytkowników do kolejek na odpowiednich piętrach
        pasazerowie.forEach(p -> blockWithElevators.addElevatorUserToQueue(p.getCurrentFloor(), p));
        towary.forEach(t -> blockWithElevators.addElevatorUserToQueue(t.getCurrentFloor(), t));
    }
}
