package cargo;

import elevator.Direction;
import floor.Floor;
import person.ElevatorUser;
import weight.Weigth;

/**
 * Klasa reprezentująca towar przewożony windą towarową.
 */
public class Cargo implements ElevatorUser {

    /**
     * Przeciętna masa towaru podana w KG.
     */
    public static final int AVERAGE_WEIGHT_OF_CARGO = 90;

    /**
     * Nazwa towaru.
     */
    private String name;

    /**
     * Waga towaru.
     */
    private Weigth weight;

    /**
     * Czy znajduje się w windzie.
     */
    private boolean isInElevator = false;

    /**
     * Na które piętro towar chce jechać.
     */
    private Floor wantToGoFloor;

    /**
     * Z którego piętra towar chce jechać.
     */
    private Floor currentFloor;

    /**
     * Konstruktor domyślny.
     */
    public Cargo() {

    }

    /**
     * Konstruktor czteroparametrowy.
     *
     * @param name          - nazwa towaru
     * @param weight        - waga towaru
     * @param currentFloor  - piętro na którym się znajduje
     * @param wantToGoFloor - piętro na które chce jechać
     */
    public Cargo(String name, Floor currentFloor, Floor wantToGoFloor, Weigth weight) {
        this.name = name;
        this.currentFloor = currentFloor;
        this.wantToGoFloor = wantToGoFloor;
        this.weight = weight;
    }

    /**
     * Zwraca nazwe towaru.
     *
     * @return name - nazwa towaru
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Ustawia nazwe towaru.
     *
     * @param name - nazwa
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Zwraca wage towaru.
     *
     * @return weight - waga towaru
     */
    @Override
    public Weigth getWeight() {
        return weight;
    }

    /**
     * Ustawia wage towaru.
     *
     * @param weight - waga
     */
    public void setWeight(Weigth weight) {
        this.weight = weight;
    }

    /**
     * Zwraca informacje czy towar znajduje się w windzie.
     *
     * @return {@true} - jest w windzie, {@false} - nie jest w windzie
     */
    @Override
    public boolean isInElevator() {
        return isInElevator;
    }

    /**
     * Ustawia informacje czy towar jest w windzie.
     *
     * @param inElevator - czy jest w windzie
     * @return this - obiekt z ustawioną wartością
     */
    @Override
    public Cargo setInElevator(boolean inElevator) {
        isInElevator = inElevator;
        return this;
    }

    /**
     * Zwraca piętro na które chce jechać towar.
     *
     * @return wantToGoFloor - piętro
     */
    @Override
    public Floor getWantToGoFloor() {
        return wantToGoFloor;
    }

    /**
     * Ustawia piętro na które chce jechać towar.
     *
     * @param wantToGoFloor - piętro
     * @return this - obiekt z ustawioną wartością
     */
    @Override
    public Cargo setWantToGoFloor(Floor wantToGoFloor) {
        this.wantToGoFloor = wantToGoFloor;
        return this;
    }

    /**
     * Zwraca piętro na którym jest towar.
     *
     * @return currentFloor - piętro
     */
    @Override
    public Floor getCurrentFloor() {
        return currentFloor;
    }

    /**
     * Ustawia piętro na którym jest towar.
     *
     * @param currentFloor - piętro
     * @return this - obiekt z ustawioną wartością
     */
    @Override
    public Cargo setCurrentFloor(Floor currentFloor) {
        this.currentFloor = currentFloor;
        return this;
    }

    /**
     * Metoda wyznacza kierunek w którym będzie jechał towar.
     *
     * @return return - wyznaczony kierunek
     */
    @Override
    public Direction getDirection() {
        Direction result = Direction.DOWN;
        if (wantToGoFloor.getFloorNo() > currentFloor.getFloorNo()) {
            result = Direction.UP;

        }
        return result;
    }

    /**
     * Zwraca string z nazwą towaru.
     *
     * @return surname - nazwa
     */
    @Override
    public String toString() {
        return name;
    }
}
