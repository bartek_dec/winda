package weight;

/**
 * Enum do przechowywania jednostek wagi.
 */
public enum WeightMetrics {
    KG, LB;

    /**
     * Współczynnik przeliczający funty na kilogramy.
     */
    public static final float LB_TO_KG_FACTOR = 0.45f;
}
