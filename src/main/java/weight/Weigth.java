package weight;


/**
 * Klasa reprezentująca wage użytkownika.
 */
public class Weigth {

    /**
     * Waga (masa).
     */
    float amount;

    /**
     * Jednostka wagi.
     */
    WeightMetrics weightMetrics;

    /**
     * Konstruktor dwuparametrowy.
     *
     * @param amount        - waga
     * @param weightMetrics - jednostka wagi
     */
    public Weigth(float amount, WeightMetrics weightMetrics) {
        this.amount = amount;
        this.weightMetrics = weightMetrics;
    }


    /**
     * Zwraca wage (mase).
     *
     * @return amount - waga
     */
    public float getAmount() {
        return amount;
    }

    /**
     * Ustawia wage (mase).
     *
     * @param amount - waga
     */
    public void setAmount(float amount) {
        this.amount = amount;
    }

    /**
     * Zwraca jednostke wagi.
     *
     * @return weightMetrics - jednostka
     */
    public WeightMetrics getWeightMetrics() {
        return weightMetrics;
    }

    /**
     * Ustawia jednostke wagi.
     *
     * @param weightMetrics - jednostka
     */
    public void setWeightMetrics(WeightMetrics weightMetrics) {
        this.weightMetrics = weightMetrics;
    }

    /**
     * Metoda ma zadanie zwrócić wagę zapisaną w obiekcie w podanej jednostce.
     *
     * @param weightMetrics - jednostka w której chcemy pobrać dane
     * @return zwraca wagę w podanej w parametrze weightMetrics jednostce
     */
    public float getWeight(WeightMetrics weightMetrics) {
        if (weightMetrics == this.weightMetrics) {
            return this.getAmount();
        } else if (this.weightMetrics == WeightMetrics.LB) {
            return this.getAmount() * WeightMetrics.LB_TO_KG_FACTOR;
        } else {
            return this.getAmount() / WeightMetrics.LB_TO_KG_FACTOR;
        }
    }

    /**
     * Zwraca informacje jaka jest masa i w jakiej jednostce.
     *
     * @return string
     */
    @Override
    public String toString() {
        return getAmount() + " " + getWeightMetrics();
    }
}
