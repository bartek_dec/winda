package address;

/**
 * Enum reprezentujący nazwy ulic.
 */
public enum StreetName {
    JÓZEFA_PONIATOWSKIEGO("Józefa Poniatowskiego"), ALEJE_RACŁAWICKIE("Aleje Racławickie"),
    JÓZEFA_SOWIŃSKIEGO("Józefa Sowińskiego"), IDZIEGO_RADZISZEWSKIEGO("Idziego Radziszewskiego"),
    PANA_TADEUSZA("Pana Tadeusza"), TOMASZA_ZANA("Tomasza Zana"),
    ALEJE_JEROZOLIMSKIE("Aleje Jerozolimskie");

    /**
     * Nazwa ulicy.
     */
    String streetName;

    /**
     * Konstruktor jednoparametrowy.
     *
     * @param streetName - nazwa ulicy
     */
    StreetName(String streetName) {
        this.streetName = streetName;
    }

    /**
     * Zwraca nazwe ulicy.
     *
     * @return streetName - nazwa ulicy
     */
    public String getStreetName() {
        return streetName;
    }

    /**
     * Zwraca string z nazwą ulicy.
     *
     * @return streetName - nazwa ulicy
     */
    @Override
    public String toString() {
        return streetName;
    }
}
