package address;

/**
 * Klasa do przechowywania adresu.
 */
public final class Address {

    /**
     * Miasto.
     */
    private City city;

    /**
     * Nazwa ulicy.
     */
    private StreetName streetName;

    /**
     * Numer budynku.
     */
    private int blockNo;

    /**
     * Numer mieszkania.
     */
    private int appartmentNo;

    /**
     * Kod pocztowy.
     */
    private String postCode;

    /**
     * Konstruktor dwuparametrowy, zakładam że nie ma sensy budowac adresu bez tych dwóch danych.
     *
     * @param city       - miasto
     * @param streetName - ulica
     */
    public Address(City city, StreetName streetName) {
        this.city = city;
        this.streetName = streetName;
    }

    /**
     * Zwraca nazwe miasta.
     *
     * @return city - zwracana nazwa miasta
     */
    public City getCity() {
        return city;
    }

    /**
     * Ustawia nazwe miasta.
     *
     * @param city - nazwa miasta
     * @return this - zwraca ten obiekt z ustawioną wartością pola
     */
    public Address setCity(City city) {
        this.city = city;
        return this;
    }

    /**
     * Zwraca nazwe ulicy.
     *
     * @return streetName - zwracana nazwa ulicy
     */
    public StreetName getStreetName() {
        return streetName;
    }

    /**
     * Ustawia nazwe ulicy.
     *
     * @param streetName - nazwa ulicy
     * @return this - zwraca ten obiekt z ustawioną wartością pola
     */
    public Address setStreetName(StreetName streetName) {
        this.streetName = streetName;
        return this;
    }

    /**
     * Zwraca numer bloku.
     *
     * @return blockNo - zwracany numer bloku
     */
    public int getBlockNo() {
        return blockNo;
    }

    /**
     * Ustawia numer bloku.
     *
     * @param blockNo - numer bloku
     * @return this - zwraca ten obiekt z ustawioną wartością pola
     */
    public Address setBlockNo(int blockNo) {
        this.blockNo = blockNo;
        return this;
    }

    /**
     * Zwraca numer mieszkania.
     *
     * @return appartmentNo - zwracany numer mieszkania
     */
    public int getAppartmentNo() {
        return appartmentNo;
    }

    /**
     * Ustawia numer mieszkania.
     *
     * @param appartmentNo - numer mieszkania
     * @return this - zwraca ten obiekt z ustawioną wartością pola
     */
    public Address setAppartmentNo(int appartmentNo) {
        this.appartmentNo = appartmentNo;
        return this;
    }

    /**
     * Zwraca kod pocztowy.
     *
     * @return postCode - zwracany kod pocztowy
     */
    public String getPostCode() {
        return postCode;
    }

    /**
     * Ustawia kod pocztowy.
     *
     * @param postCode - kod pocztowy
     * @return this - zwraca ten obiekt z ustawioną wartością pola
     */
    public Address setPostCode(String postCode) {
        this.postCode = postCode;
        return this;
    }

    /**
     * Wyświetla adres.
     *
     * @return string - adres zawierający nazwe miasta, ulicy i numer bloku
     */
    @Override
    public String toString() {
        return getCity() + " ul. " + getStreetName() + " " + getBlockNo();
    }
}
