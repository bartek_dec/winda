package address;

/**
 * Enum do przechowywania nazwy miasta i liczby ich mieszkańców.
 */
public enum City {

    LUBLIN("Lublin", 342000), WARSZAWA("Warszawa", 1735000), GDAŃSK("Gdańsk", 456000),
    ŁÓDŹ("Łódź", 701000), STALOWA_WOLA("Stalowa Wola", 63000), KATOWICE("Katowice", 304000);

    /**
     * Nazwa miasta.
     */
    private String cityName;

    /**
     * Liczba mieszkańców miasta.
     */
    private int population;

    /**
     * Konstruktor dwuparametrowy.
     *
     * @param cityName   - nazwa miasta
     * @param population - liczba mieszkańcoów
     */
    City(String cityName, int population) {
        this.cityName = cityName;
        this.population = population;
    }

    /**
     * Zwraca mazwe miasta.
     *
     * @return cityName - nazwa miasta
     */
    public String getCityName() {
        return cityName;
    }

    /**
     * Zwraca liczbę mieszkańców miasta.
     *
     * @return population - liczba mieszkańców
     */
    public int getPopulation() {
        return population;
    }

    /**
     * Zwraca string z nazwą miasta.
     *
     * @return cityName - nazwa miasta
     */
    @Override
    public String toString() {
        return cityName;
    }

    /**
     * Zwraca string z informacjami o mieście.
     *
     * @return string - informacje o mieście
     */
    public String getInfoAboutCity() {
        return cityName + " ma " + population + " mieszkańców.";
    }
}
