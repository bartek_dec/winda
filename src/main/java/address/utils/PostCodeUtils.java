package address.utils;

import com.sun.istack.internal.Nullable;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Klasa narzędziowa służąca do weryfikacji poprawnosci kodu pocztowego,
 * final -  blokuje możliwość dzedziczenia z tej klasy.
 */
public final class PostCodeUtils {

    /**
     * Konstruktor prywatny blokuje możliwość utworzenia obiektu tej klasy spoza klasy.
     */
    private PostCodeUtils() {

    }

    /**
     * Metoda ma za zadanie sprawdzenie poprawności przekazanego parametrem {@code aCode}
     * kodu pocztowego w zadanym parametrem {@code aRegion} regionie. Jeżeli parametr
     * {@code aRegion} jest {@code null} to region zostanie pobrany z ustawień systemu
     *
     * @param code   - kod pocztowy
     * @param region - region
     * @return {@code true} jeżeli kod jest poprawny, {@code false} w przeciwnym razie
     */
    public static boolean isCorrect(String code, @Nullable String region) {
        boolean result = false;

        if (region == null) {
            Locale locale = Locale.getDefault();
            region = locale.getISO3Country();
        }

        Pattern pattern = null;

        switch (region) {
            case "POL":
                pattern = Pattern.compile("\\d{2}-\\d{3}");
                break;

            case "USA":
                pattern = Pattern.compile("\\d{5}");
                break;

            case "NLD":
                pattern = Pattern.compile("\\d{4} [A-Z]{2}");
                break;

            case "JPN":
                pattern = Pattern.compile("\\d{3}-\\d{4}");
                break;

            case "CAN":
                pattern = Pattern.compile("[A-Z]\\d[A-Z] \\d[A-Z]\\d");
                break;
        }

        if (pattern != null) {
            Matcher matcher = pattern.matcher(code);
            result = matcher.matches();
        }

        return result;
    }
}
