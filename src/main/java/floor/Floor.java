package floor;

/**
 * Klasa reprezenująca piętro.
 */
public class Floor {

    /**
     * Numer piętra.
     */
    private Integer floorNo;

    /**
     * Konstruktor jednoparametrowy.
     *
     * @param floorNo - numer piętra
     */
    public Floor(Integer floorNo) {
        this.floorNo = floorNo;
    }

    /**
     * Zwraca numer piętra.
     *
     * @return floorNo - numer piętra
     */
    public Integer getFloorNo() {
        return floorNo;
    }

    /**
     * Ustawia numer piętra.
     *
     * @param floorNo - numer piętra
     */
    public void setFloorNo(Integer floorNo) {
        this.floorNo = floorNo;
    }

    /**
     * Metoda zwraca informacje czy porównywane piętra są sobie równe.
     *
     * @param obj - porównywane piętro
     * @return {@true} - piętra są sobie równe, {@false} - piętra nie są równe
     */
    @Override
    public boolean equals(Object obj) {
        boolean result = false;

        if (obj instanceof Floor) {
            Floor temp = (Floor) obj;
            if (floorNo == temp.floorNo) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Metoda zwraca hashCode dla danego numeru piętra.
     *
     * @return hashCode - hashCode danego numeru piętra.
     */
    @Override
    public int hashCode() {
        return Integer.hashCode(floorNo);
    }

    /**
     * Metoda numer piętra o jeden.
     */
    public void upFloor() {
        floorNo++;
    }

    /**
     * Metoda zmniejsza numer piętra o jeden.
     */
    public void downFloor() {
        floorNo--;
    }

    /**
     * Metoda zwraca string z numerem piętra.
     *
     * @return string - numer piętra
     */
    @Override
    public String toString() {
        return String.valueOf(floorNo);
    }
}
