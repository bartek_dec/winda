package utils;

import cargo.Cargo;
import com.google.gson.Gson;
import person.ElevatorUser;
import person.Passenger;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JsonUtil {

    private Gson gson = new Gson();

    /**
     * Metoda zapisuje obiekt Json do pliku tekstowego (obiekt jest
     * dodawany w nowej linii jeśli plik już istnieje lub tworzy nowy
     * plik i zapisuje w nim obiekt Json jeśli plik jeszcze nie istnieje).
     *
     * @param type - typ obiektu zapisywany do obiektu Json
     * @param path - ścieżka do pliku
     */
    public void writeJsonToFile(ElevatorUser type, String path) {

        String json = gson.toJson(type);

        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(path),
                StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND)) {

            writer.write(json);
            writer.newLine();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Metoda zamienia stringi reprezentujące obiekty Json na obiekty odpowiedniego typu.
     *
     * @param classType - typ obiektu na jaki mają być zamienione obiekty Json
     * @param path      - ścieżka do pliku
     * @return output - kolekcja obiektów odpowiedniego typu
     */
    public List<ElevatorUser> readJsons(String classType, String path) {
        List<String> inputs = readFromFile(path);
        List<ElevatorUser> output = new ArrayList<>();

        if ("passenger".equals(classType.toLowerCase())) {
            for (String input : inputs) {
                ElevatorUser user = gson.fromJson(input, Passenger.class);
                output.add(user);
            }
            return output;

        } else if ("cargo".equals(classType.toLowerCase())) {
            for (String input : inputs) {
                ElevatorUser user = gson.fromJson(input, Cargo.class);
                output.add(user);
            }
            return output;

        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Metoda odczytuje plik tekstowy i zapisuje do kolekcji stringi reprezentujące
     * obiekty Json.
     *
     * @param path - ścieżka do pliku
     * @return output - kolekcja z zapisanymi stringami
     */
    private List<String> readFromFile(String path) {

        List<String> output = new ArrayList<>();

        try (Stream<String> strings = Files.lines(Paths.get(path))) {

            output = strings.collect(Collectors.toList());

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return output;
    }
}
