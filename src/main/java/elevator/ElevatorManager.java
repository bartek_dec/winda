package elevator;

import block.BlockWithElevators;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Zarządca wind w bloku.
 */
public class ElevatorManager {

    /**
     * blok z windami.
     */
    private BlockWithElevators blockWithElevators;

    /**
     * Czy zarządca pracuje.
     */
    AtomicBoolean isWorking = new AtomicBoolean(false);


    /**
     * Konstruktor jednoparametrowy.
     *
     * @param blockWithElevators - blok z windami
     */
    public ElevatorManager(BlockWithElevators blockWithElevators) {
        this.blockWithElevators = blockWithElevators;
    }

    /**
     * Zwraca blok z windami.
     *
     * @return blockWithElevators - blok z windami
     */
    public BlockWithElevators getBlockWithElevators() {
        return blockWithElevators;
    }

    /**
     * Ustawia blok z windami.
     *
     * @param blockWithElevators - blok z windami
     */
    public void setBlockWithElevators(BlockWithElevators blockWithElevators) {
        this.blockWithElevators = blockWithElevators;
    }


    /**
     * Metoda uruchamia managera w oddzielnym wątku.
     *
     * @return {@false} - manager był już uruchomiony,
     * {@true} - manager został uruchomiony
     */
    public boolean startWork() {

        if (isWorking.get()) {
            return false;
        }

        isWorking.set(true);
        new Thread(() -> {
            while (isWorking.get()) {
                moveElevators();
                printState();
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {

                }
            }
        }).start();
        return true;
    }

    /**
     * Metoda zatrzymuje managera.
     */
    public void stopWork() {
        isWorking.set(false);
    }

    /**
     * Metoda uruchamia każdą z wind.
     */
    private void moveElevators() {
        List<Elevator> elevators = blockWithElevators.getElevators();

        elevators.forEach(elevator -> elevator.nextStep());
    }

    /**
     * Metoda wyświetla stan w jakim jest dana winda.
     */
    public void printState() {
        List<Elevator> elevators = blockWithElevators.getElevators();

        elevators.forEach(elevator -> System.out.println(elevator.toString()));
    }

}

