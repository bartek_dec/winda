package elevator;

/**
 * Reprezentuje stan w jakim jest winda: {@code EXIT} - wychodzą, {@code ENTER} - wchodzą,
 * {@code MOVE} - porusza się, {@code STOP} - stoi.
 */
public enum State {
    EXIT, ENTER, MOVE, STOP
}
