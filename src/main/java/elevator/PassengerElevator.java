package elevator;

import person.Passenger;
import weight.WeightMetrics;
import weight.Weigth;

/**
 * Klasa reprezentująca windę osobową.
 */
public class PassengerElevator extends Elevator {

    /**
     * Stała służąca do przeliczania maksymalnego
     * udźwigu windy na bezpieczny udźwig windy.
     */
    public static final float PASSENGER_SAFETY_WEIGHT_FACTOR = 0.9F;

    /**
     * Konstruktor domyślny.
     */
    public PassengerElevator() {

    }

    /**
     * Metoda oblicza maksymalną ilość pasażerów, którzy
     * mogą być przewożoni za jednym razem.
     *
     * @param maxLoad - maksymalny udźwig windy
     */
    @Override
    public void setMaxLoad(Weigth maxLoad) {
        super.setMaxLoad(maxLoad);

        float safeLoad = PASSENGER_SAFETY_WEIGHT_FACTOR * maxLoad.getAmount();
        float typicalPersonWeight = Passenger.AVERAGE_WEIGHT_OF_PASSENGER;

        if (maxLoad.getWeightMetrics() == WeightMetrics.LB) {
            typicalPersonWeight /= WeightMetrics.LB_TO_KG_FACTOR;
        }
        maxNumberOfUsers = (short) (safeLoad / typicalPersonWeight);
    }
}
