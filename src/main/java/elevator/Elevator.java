package elevator;

import block.QueueChecker;
import cargo.Cargo;
import floor.Floor;
import person.ElevatorUser;
import person.Passenger;
import utils.Utils;
import weight.Weigth;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.function.Predicate;

/**
 * Klasa reprezentująca windę.
 */
public class Elevator {

    /**
     * Współczynnik bezpieczeństwa (ilość w kilogramach o którą
     * zmniejsza się dopuszczalny udźwig windy podczas jej inicjalizacji).
     */
    public static final float ELEVATOR_SAFE_FACTOR = 100F;

    /**
     * Lista pięter obsługiwanych przez windę.
     */
    protected List<Floor> availableFloors;

    /**
     * Piętro na którym znajduje się winda.
     */
    protected Floor currentFloor;

    /**
     * Czy winda jest w trakcie przeglądu - jest niedostępna.
     */
    protected boolean serviceBreak = false;

    /**
     * Maksymalny udźwig windy.
     */
    protected Weigth maxLoad;

    /**
     * W którą stronę porusza się winda.
     */
    protected Direction direction = Direction.NONE;

    /**
     * W jakim stanie jest winda.
     */
    protected State state = State.STOP;

    /**
     * Żródło danych o nowych rządaniach, które sa do obsłużenia.
     */
    protected QueueChecker queueChecker;

    /**
     * Kolejka żądań wysyłanych przez oczekujących użytkowników
     * i odbierana przez winde.
     */
    protected Deque<Floor> requestedFloors;

    /**
     * Lista użytkowników na pokładzie.
     */
    protected List<ElevatorUser> usersOnBoard = new ArrayList<>();

    /**
     * Maksymalna liczba użytkowników w windzie.
     */
    protected short maxNumberOfUsers;

    /**
     * Całkowita waga użytkowników znajdujących się aktualnie w windzie.
     */
    protected float maxLoadCounter = 0F;


    /**
     * Ilość użytkowników znajdujących się aktualnie w windzie.
     */
    protected short numberOfUsersCounter = 0;

    /**
     * Konstruktor domyślny.
     */
    public Elevator() {
        requestedFloors = new ArrayDeque<>();
    }

    /**
     * Zwraca kolejke żądań wysłanych przez użytkowników.
     *
     * @return requestedFloors - kolejka żądań
     */
    public synchronized Deque<Floor> getRequestedFloors() {
        return requestedFloors;
    }


    /**
     * Ustawia kolejke żądań wysłanych przez użytkowników.
     *
     * @param requestedFloors - kolejka żądań
     */
    public synchronized void setRequestedFloors(Deque<Floor> requestedFloors) {
        this.requestedFloors = requestedFloors;
    }


    /**
     * Zwraca źrudło danych o nowych rządaniach, które sa dostępne.
     *
     * @return queueChecker - żródło danych
     */
    public QueueChecker getQueueChecker() {
        return queueChecker;
    }

    /**
     * Ustawia źródło danych o nowych rządaniach, które sa dostępne.
     *
     * @param queueChecker - źródło danych
     */
    public void setQueueChecker(QueueChecker queueChecker) {
        this.queueChecker = queueChecker;
    }

    /**
     * Zwraca liste pięter obsługiwanych przez winde.
     *
     * @return availableFloors - lista obsługiwanych pięter
     */
    public List<Floor> getAvailableFloors() {
        return availableFloors;
    }

    /**
     * Ustawia liste pięter obsługiwanych przez winde.
     *
     * @param availableFloors - lista pięter
     */
    public void setAvailableFloors(List<Floor> availableFloors) {
        this.availableFloors = availableFloors;
    }

    /**
     * Zwraca piętro na którym znajduje się winda.
     *
     * @return currentFloor - aktualne piętro
     */
    public Floor getCurrentFloor() {
        return currentFloor;
    }

    /**
     * Ustawia piętro na którym znajduje się winda.
     *
     * @param currentFloor - aktualne piętro
     */
    public void setCurrentFloor(Floor currentFloor) {
        this.currentFloor = currentFloor;
    }

    /**
     * Zwraca informacje czy winda jest dostępna.
     *
     * @return {@false} - gdy winda jest dostępna,
     * {@true} - gdy jest niedostępna
     */
    public boolean isServiceBreak() {
        return serviceBreak;
    }

    /**
     * Ustawia informacje o tym czy winda jest dostępna czy nie.
     *
     * @param serviceBreak - informacja o dostępności windy
     */
    public void setServiceBreak(boolean serviceBreak) {
        this.serviceBreak = serviceBreak;
    }

    /**
     * Zwraca informacje o dopuszczalnym obciążeniu windy.
     *
     * @return maxLoad - dopuszczalne obciążenia
     */
    public Weigth getMaxLoad() {
        return maxLoad;
    }

    /**
     * Ustawia dopuszczalny udźwig windy.
     *
     * @param maxLoad - udźwig windy podawany podczas inicjalizacji
     */
    public void setMaxLoad(Weigth maxLoad) {
        Weigth safeWeight = new Weigth(maxLoad.getWeight(maxLoad.getWeightMetrics())
                - ELEVATOR_SAFE_FACTOR, maxLoad.getWeightMetrics());
        this.maxLoad = safeWeight;
    }

    /**
     * Zwraca informacje o kierunku w którym porusza się winda.
     *
     * @return direction - kierunek ruchu
     */
    public Direction getDirection() {
        return direction;
    }

    /**
     * Ustawia kierunek poruszania się windy.
     *
     * @param direction - kierunek ruchu
     */
    public void setDirection(Direction direction) {
        this.direction = direction;
    }


    /**
     * Zwraca informacje w jakim stanie jest winda.
     *
     * @return state - stan w jakim jest winda: {@code EXIT} - wychodzą,
     * {@code ENTER} - wchodzą, {@code MOVE} - porusza się, {@code STOP} - stoi
     */
    public State getState() {
        return state;
    }

    /**
     * Lista użytkowników znajdujących się w windzie.
     *
     * @return usersOnBoard - lista użytkowników jest w windzie
     */
    public List<ElevatorUser> getUsersOnBoard() {
        return usersOnBoard;
    }

    /**
     * Ustawia liste użytkowników na pokładzie windy.
     *
     * @param usersOnBoard - użytkownicy w windzie
     */
    public void setUsersOnBoard(List<ElevatorUser> usersOnBoard) {
        this.usersOnBoard = usersOnBoard;
    }

    /**
     * Zwraca informacje ilu maksymalnie użytkowników winda może pomieścić.
     *
     * @return maxNumberOfUsers - maksymalna ilość użytkowników
     */
    public short getMaxNumberOfUsers() {
        return maxNumberOfUsers;
    }

    /**
     * Zwraca sumaryczną wage użytkowników znajdujących się w windzie.
     *
     * @return maxLoadCounter - całkowita waga użytkowników w windzie
     */
    public float getMaxLoadCounter() {
        return maxLoadCounter;
    }

    /**
     * Zwraca informacje ilu użytkowników znajduje się aktualnie w windzie.
     *
     * @return numberOfUsersCounter - ilość użytkowników w windzie
     */
    public short getNumberOfUsersCounter() {
        return numberOfUsersCounter;
    }

    /**
     * Zwraca informacje o windzie.
     *
     * @return msg - string z informacjami o windzie
     */
    @Override
    public String toString() {
        StringBuilder msg = new StringBuilder("Jestem: ")
                .append(super.toString())
                .append(" moje piętro: ")
                .append(currentFloor.getFloorNo())
                .append(". Kierunek: ")
                .append(direction.toString());

        return msg.toString();
    }


    /**
     * Metoda wykonuje czynności windy.
     */
    public void nextStep() {
        // pierwszy przypadek - ludzie wychodzą z windy
        if (anybodyWantExitElevatorOnCurrentFloor()) {
            state = State.EXIT;
            exitFromElevator();
        }
        // drugi przypadek - ludzie wchodza do windy
        else if (anybodyWantEnterElevatorOnCurrentFloor()) {
            direction = calculateElevatorDirection();
            if (requestMatchCurrentFloor()) {
                state = State.ENTER;
                enterToElevator();
            } else if ((theSameDirectionAsElevator() && smallerOrEqualCurrentFloor())
                    || (theSameDirectionAsElevator() && bothUsersInTheSameDirection())) {
                state = State.ENTER;
                enterToElevator();
            } else {
                move();
            }
        }

        // trzeci przypadek - winda kogoś przewozi
        else if (anybodyOnBoard()) {
            move();
        }

        // pusta winda otrzymuje wezwanie od użytkownika
        else if (anybodyRequestElevator()) {
            direction = calculateElevatorDirection();
            if (direction == Direction.NONE) {
                requestedFloors.removeFirst();
            } else {
                move();
            }
        }

        // nic się nie dzieje - winda odpoczywa
        else {
            direction = Direction.NONE;
            state = State.STOP;

            if (Utils.SHOW_LOG) {
                System.out.println("Winda czeka na piętrze: " + currentFloor
                        + " (brak chętnych do jazdy).");
            }
        }
    }

    /**
     * wyznacza kierunek ruchu windy.
     *
     * @return {@code DOWN} - do dołu, {@code UP} - do góry {@code NONE} - nie porusza sie
     */
    private synchronized Direction calculateElevatorDirection() {
        if (requestedFloors.peekFirst() != null && requestedFloors.peekFirst().getFloorNo() > currentFloor.getFloorNo()) {
            return Direction.UP;
        } else if (requestedFloors.peekFirst() != null && requestedFloors.peekFirst().getFloorNo() < currentFloor.getFloorNo()) {
            return Direction.DOWN;
        } else {
            return Direction.NONE;
        }
    }

    /**
     * Zwraca informacje czy ktoś wzywa winde.
     *
     * @return {@true} - ktoś wzywa winde, {@false} - nie ma wezwań
     */
    private synchronized boolean anybodyRequestElevator() {

        return requestedFloors.stream().anyMatch(new Predicate<Floor>() {
            @Override
            public boolean test(Floor floor) {
                if (!requestedFloors.isEmpty()) {
                    return true;
                } else {
                    return false;
                }
            }
        });

    }

    /**
     * Przemieszcza winde.
     */
    private void move() {
        if (direction == Direction.UP) {
            currentFloor.upFloor();
        } else {
            currentFloor.downFloor();
        }

        if (Utils.SHOW_LOG) {
            System.out.println("Winda " + super.toString() + " jedzie: "
                    + direction + " na piętro: " + currentFloor);
        }
    }

    /**
     * Zwraca informacje czy jakikolwiek użytkownik przebywa w windzie.
     *
     * @return {@true} - ktoś jest w windzie, {@false} - winda jest pusta
     */
    private boolean anybodyOnBoard() {
        return usersOnBoard.size() != 0;
    }

    /**
     * Dodaje użytkownika do windy (użytkownik wsiada do windy).
     */
    private void enterToElevator() {
        //pobiera liste osób czekających na piętrze
        List<ElevatorUser> queue = queueChecker.getQueue(getCurrentFloor());
        //lista osób w odwróconej kolejności
        List<ElevatorUser> reversedQueue = reversQueueOnFloor(queue);
        int size = reversedQueue.size();
        ElevatorUser elevatorUser;

        for (int i = size - 1; i >= 0; i--) {

            elevatorUser = reversedQueue.get(i);

            if (direction == Direction.NONE || usersOnBoard.size() == 0) {
                direction = elevatorUser.getDirection();
            }

            //jeśli osoba jedzie w tą samą strone co winda to wsiada do windy
            if (elevatorUser.getDirection() == direction) {
                increaseNumberOfUsersCounter();
                increaseMaxLoadCounter(elevatorUser);

                if (belowMaxNumberOfUsers() && belowMaxLoad()) {
                    if (elevatorUser instanceof Passenger && this instanceof PassengerElevator) {

                        userDoesMoveIntoElevator(elevatorUser);

                        System.out.println("UŻYTKOWNIK: " + elevatorUser
                                + " WCHODZI do windy na piętrze: " + currentFloor);
                    } else if (elevatorUser instanceof Cargo && this instanceof ServiceElevator) {

                        userDoesMoveIntoElevator(elevatorUser);

                        System.out.println("TOWAR: " + elevatorUser
                                + " ZOSTAŁ ZAŁADOWANY do windy na piętrze: " + currentFloor);
                    } else {
                        decreaseNumberOfUsersCounter();
                        decreaseMaxLoadCounter(elevatorUser);
                        requestedFloors.remove(currentFloor);
                    }
                } else {
                    decreaseNumberOfUsersCounter();
                    decreaseMaxLoadCounter(elevatorUser);
                }
            }
        }
    }

    /**
     * Metoda zmienia położenie użytkownika (użytkownik opuszcza kolejke i wchodzi do windy).
     *
     * @param elevatorUser - użytkownik
     */
    private synchronized void userDoesMoveIntoElevator(ElevatorUser elevatorUser) {
        usersOnBoard.add(elevatorUser);
        queueChecker.removePersonFromQueue(elevatorUser);
        elevatorUser.setInElevator(true);
        requestedFloors.remove(currentFloor);
    }

    /**
     * Zwraca informacje czy jakiś użytkownik chce wsiąść do windy na danym piętrze.
     *
     * @return {@true} - ktoś chce wsiąść, {@false} - nikt nie chce wsiadać
     */
    private boolean anybodyWantEnterElevatorOnCurrentFloor() {
        boolean anybodyGoToElevator = false;

        if (queueChecker.isAnyQueueOnFloor(getCurrentFloor())) {

            if (direction == Direction.NONE && requestedFloors.size() > 0
                    || usersOnBoard.size() == 0 && requestedFloors.size() > 0) {
                anybodyGoToElevator = true;
            } else {
                List<ElevatorUser> queue = queueChecker.getQueue(getCurrentFloor());
                anybodyGoToElevator = queue.stream()
                        .anyMatch(elevatorUser -> elevatorUser.getDirection() == direction);
            }
        }

        return anybodyGoToElevator;
    }

    /**
     * Zwraca informacje czy jakiś użytkownik chce wysiąść z windy na danym piętrze.
     *
     * @return {@true} - ktoś chce wysiąść, {@false} - nikt nie chce wysiadać
     */
    private boolean anybodyWantExitElevatorOnCurrentFloor() {
        return usersOnBoard.stream().anyMatch(elevatorUser -> elevatorUser
                .getWantToGoFloor().equals(currentFloor));
    }

    /**
     * Użytkownik wysiada (metoda usuwa użytkownika z windy).
     */
    private void exitFromElevator() {
        int size = usersOnBoard.size();
        ElevatorUser elevatorUser;
        for (int i = size - 1; i >= 0; i--) {
            elevatorUser = usersOnBoard.get(i);
            if (elevatorUser.getWantToGoFloor().equals(currentFloor)) {

                userDoesMoveOutOfElevator(elevatorUser, i);

                if (Utils.SHOW_LOG) {
                    if (elevatorUser instanceof Passenger) {
                        System.out.println("UŻYTKOWNIK: " + elevatorUser
                                + " WYCHODZI z windy na piętrze: " + currentFloor);
                    } else if (elevatorUser instanceof Cargo) {
                        System.out.println("TOWAR: " + elevatorUser
                                + " ZOSTAŁ WYŁADOWANY z windy na piętrze: " + currentFloor);
                    }
                }
            }
        }
        //jeśli winda jest pusta i nikt jej nie wzywa to czyszcze liste z żądaniami
        if (usersOnBoard.size() == 0 && !anybodyRequestElevator()) {
            requestedFloors.clear();
        }
    }

    /**
     * Metoda zmienia położenie użytkownika (użytkownik opuszcza winde).
     *
     * @param elevatorUser - użytkownik
     * @param i            - index
     */
    private synchronized void userDoesMoveOutOfElevator(ElevatorUser elevatorUser, int i) {
        usersOnBoard.remove(i);
        decreaseMaxLoadCounter(elevatorUser);
        decreaseNumberOfUsersCounter();
        elevatorUser.setInElevator(false);
    }

    /**
     * Metoda sprawdza czy piętro z którego przybyło wezwanie pokrywa się z piętrem
     * na którym znajduje się aktualnie winda.
     *
     * @return {@true} - piętra się pokrywają, {@false} - piętra się nie pokrywają
     */
    private boolean requestMatchCurrentFloor() {
        if (requestedFloors.peekFirst() != null && requestedFloors.peekFirst().getFloorNo() == currentFloor.getFloorNo()) {
            return true;
        }
        return false;
    }

    /**
     * Metoda sprawdza czy użytkownik napotkany po drodze chce jechać w tą samą
     * stronę co jadąca winda.
     *
     * @return {@true} - winda i napotkany użytkownik chcą jechać w tą samą stronę,
     * {@false} - winda i napotkany użytkownik chcą jechać w przeciwnych kierunkach
     */
    private boolean theSameDirectionAsElevator() {
        return queueChecker.getQueue(currentFloor).stream().anyMatch(new Predicate<ElevatorUser>() {
            @Override
            public boolean test(ElevatorUser elevatorUser) {
                if (elevatorUser.getDirection() == direction) {
                    return true;
                } else {
                    return false;
                }
            }
        });
    }

    /**
     * Metoda sprawdza czy użytkownik, który wezwał winde jako drugi w kolejności
     * (lub w dalszej kolejności) jedzie w tym samym kierunku co użytkownik,
     * który wezwał winde jako pierwszy.
     *
     * @return {@true} - obaj użytkownicy jadą w tym samym kierunku
     * {@false} - obaj użytkownicy jadą w przeciwnych kierunkach
     */
    private boolean bothUsersInTheSameDirection() {
        boolean result = false;
        try {
            Direction majorRequestorDirection = queueChecker.getQueue(requestedFloors
                    .getFirst()).get(0).getDirection();

            Direction minorRequestorDirection = queueChecker.getQueue(currentFloor)
                    .get(0).getDirection();

            if (minorRequestorDirection == majorRequestorDirection) {
                result = true;
            }
        } catch (IndexOutOfBoundsException ex) {
            requestedFloors.removeFirst();
            result = false;
        }
        return result;
    }

    /**
     * Metoda sprawdza czy użytkownik, który wezwał winde jako drugi w kolejności
     * (lub w dalszej kolejności) jedzie na to samo piętro lub niższe niż piętro
     * na którym jest użytkownik, który wezwał winde jako pierwszy.
     *
     * @return {@true} - gdy użytkownik który wezwał winde jako drugi (lub w dalszej
     * kolejności) jedzie na niższe piętro lub to samo co użytkownik, który wezwał
     * winde jako pierwszy, {@false} - w przeciwnym warunku
     */
    private boolean smallerOrEqualCurrentFloor() {
        int majorRequestorCurrentFloor = requestedFloors.getFirst().getFloorNo();

        int minorRequestorRequestedFloor = queueChecker.getQueue(currentFloor)
                .get(0).getWantToGoFloor().getFloorNo();

        if (minorRequestorRequestedFloor <= majorRequestorCurrentFloor) {
            return true;
        }
        return false;
    }

    /**
     * Dodaje wage użytkownika wchodzącego do windy do licznika wagi.
     *
     * @param elevatorUser - użytkownik wchodzący do windy
     */
    private void increaseMaxLoadCounter(ElevatorUser elevatorUser) {
        maxLoadCounter += elevatorUser.getWeight().getAmount();
    }

    /**
     * Odejmuje wage użytkownika wychodzącego z windy od licznika wagi.
     *
     * @param elevatorUser - użytkownik wychodzący z windy
     */
    private void decreaseMaxLoadCounter(ElevatorUser elevatorUser) {
        maxLoadCounter -= elevatorUser.getWeight().getAmount();
    }

    /**
     * Zwiększa wartość licznika użytkowników przebywających w windzie.
     */
    private void increaseNumberOfUsersCounter() {
        numberOfUsersCounter++;
    }

    /**
     * Zmniejsza wartość licznika użytkowników przebywających w windzie.
     */
    private void decreaseNumberOfUsersCounter() {
        numberOfUsersCounter--;
    }

    /**
     * Sprawdza czy w windzie jest dopuszczalna liczba użytkowników.
     *
     * @return {@false} - gdy użytkowników jest za dużo
     */
    private boolean belowMaxNumberOfUsers() {
        return numberOfUsersCounter <= maxNumberOfUsers;
    }

    /**
     * Sprawdza czy całkowity ciężar użytkowników w windzie jest poniżej udźwigu windy.
     *
     * @return {@false} - gdy ciężar użytkowników przekracza udźwig windy
     */
    private boolean belowMaxLoad() {
        return maxLoadCounter <= maxLoad.getAmount();
    }

    /**
     * Metoda odwraca kolejność użytkowników czekających na winde na danym piętrze.
     *
     * @param queue - kolejka użytkowników czekających na winde
     * @return reversedList - kolejka użytkowników w odwróconej kolejności
     */
    private List<ElevatorUser> reversQueueOnFloor(List<ElevatorUser> queue) {
        List<ElevatorUser> reversedList = new ArrayList<>();
        int size = queue.size();

        for (int i = size - 1; i >= 0; i--) {
            reversedList.add(queue.get(i));
        }
        return reversedList;
    }
}
