package elevator;

/**
 * Enum definiujący możliwe kierunki poruzania się windy {@code UP} - do góry,
 * {@code DOWN} - do dołu, {@code NONE} - nie zdefiniowany.
 */
public enum Direction {
    DOWN, UP, NONE
}
