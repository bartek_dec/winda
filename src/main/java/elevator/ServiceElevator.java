package elevator;

import cargo.Cargo;
import weight.WeightMetrics;
import weight.Weigth;

/**
 * Klasa reprezentująca windę towarową.
 */
public class ServiceElevator extends Elevator {


    /**
     * Stała służąca do przeliczania maksymalnego
     * udźwigu windy na bezpieczny udźwig windy.
     */
    public static final float CARGO_SAFETY_WEIGHT_FACTOR = 0.85F;

    /**
     * Konstruktor domyślny.
     */
    public ServiceElevator() {

    }

    /**
     * Metoda oblicza maksymalną ilość sztuk towarów, które
     * mogą być przewożone za jednym razem.
     *
     * @param maxLoad - maksymalny udźwig windy
     */
    @Override
    public void setMaxLoad(Weigth maxLoad) {
        super.setMaxLoad(maxLoad);

        float safeLoad = CARGO_SAFETY_WEIGHT_FACTOR * maxLoad.getAmount();
        float typicalCargoWeight = Cargo.AVERAGE_WEIGHT_OF_CARGO;

        if (maxLoad.getWeightMetrics() == WeightMetrics.LB) {
            typicalCargoWeight /= WeightMetrics.LB_TO_KG_FACTOR;
        }
        maxNumberOfUsers = (short) (safeLoad / typicalCargoWeight);
    }
}
