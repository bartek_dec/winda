package block;

import floor.Floor;
import person.ElevatorUser;
import person.Passenger;

import java.util.List;

/**
 * Interfejs który ma być żródłem danych dla windy o żądaniach jakie sa do obsłużenia.
 */
public interface QueueChecker {

    /**
     * Metoda ma za zadanie zwrócić informację, czy na piętrze zadanym parametrem
     * {@code floor} ktos oczekuję.
     *
     * @param floor - piętro na którym sprawdzamy kolejkę
     * @return {@code true} jeżeli czeka jakakolwiek osoba na tym piętrze,
     * {@code false} w przeciwnym razie
     */
    boolean isAnyQueueOnFloor(Floor floor);

    /**
     * Metoda ma za zadanie zwórcić informację o osobach cozekujących na windę na konkretnym
     * piętrze zadanym parametrem {@code floor}.
     *
     * @param floor - piętro na którym sprawdzamy kolejkę
     * @return Lista osób czekająca na windę, lub {@code null} jeżeli nikt nie oczekuje
     */
    List<ElevatorUser> getQueue(Floor floor);

    /**
     * Ma za zadanie usunąć pasażera {@code passenger} z kolejki.
     *
     * @param elevatorUser - użytkownik który ma zostać usunięty
     */
    void removePersonFromQueue(ElevatorUser elevatorUser);
}
