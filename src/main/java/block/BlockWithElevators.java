package block;

import java.util.*;

import cargo.Cargo;
import elevator.Elevator;
import elevator.PassengerElevator;
import elevator.ServiceElevator;
import floor.Floor;
import person.ElevatorUser;
import person.Passenger;


/**
 * Klasa reprezentująca blok z windami.
 */
public class BlockWithElevators extends Block implements QueueChecker {

    /**
     * Lista dostepych w bloku wind.
     */
    private LinkedList<Elevator> elevators;

    /**
     * Kolejka do windy na odpowiednim piętrze.
     */
    private HashMap<Floor, List<ElevatorUser>> elevatorQueue;

    /**
     * Informacja o tym czy użytkownik jest za ciężki do przewiezienia windą.
     */
    private boolean lightUser = true;

    /**
     * Metoda ustawia informacje o tym czy użytkownik jest za ciężki dla windy.
     *
     * @param lightUser - informacja
     */
    public void isUserHeavy(boolean lightUser) {
        this.lightUser = lightUser;
    }


    /**
     * Konstruktor jednoparametrowy.
     *
     * @param floors - lista pięter w budynku
     */
    public BlockWithElevators(List<Floor> floors) {
        super(floors);
        elevatorQueue = new HashMap<>();
        elevators = new LinkedList<>();
    }

    /**
     * Zwraca liste dostępnych w bloku wind.
     *
     * @return elevators - lista wind
     */
    public LinkedList<Elevator> getElevators() {
        return elevators;
    }

    /**
     * Metoda ustawia liste wind dostępnych w bloku.
     *
     * @param elevators - lista wind
     */
    public void setElevators(LinkedList<Elevator> elevators) {
        this.elevators = elevators;
        elevators.stream().forEach(elevator -> elevator.setQueueChecker(this));
    }

    /**
     * Zwraca kolejke użytkowników oczekujących na winde na danym piętrze.
     *
     * @return elevatorQueue - kolejka na danym piętrze
     */
    public HashMap<Floor, List<ElevatorUser>> getElevatorQueue() {
        return elevatorQueue;
    }

    /**
     * Ustawia kolejke użytkowników oczekujących na winde na danym piętrze.
     *
     * @param elevatorQueue - kolejka na danym piętrze
     */
    public void setElevatorQueue(HashMap<Floor, List<ElevatorUser>> elevatorQueue) {
        this.elevatorQueue = elevatorQueue;
    }

    /**
     * Metoda zwracająca informację czy w bloku jest winda.
     *
     * @return {@code true} - jeżeli jest winda, {@code false} w przeciwnym razie
     */
    @Override
    public boolean hasElevator() {
        return true;
    }

    /**
     * Dodaje użytkownika windy do kolejki.
     *
     * @param currentFloor - piętro na którym zostanie dodany użytkownik
     * @param elevatorUser - uzytkownik który zostanie dodany
     */
    public void addElevatorUserToQueue(Floor currentFloor, ElevatorUser elevatorUser) {

        List<ElevatorUser> listOfWaitingPeopleOnFloor = elevatorQueue.get(currentFloor);

        if (listOfWaitingPeopleOnFloor == null) {
            listOfWaitingPeopleOnFloor = new ArrayList<>();
            elevatorQueue.put(currentFloor, listOfWaitingPeopleOnFloor);
        }

        if (isFloorInBuilding(elevatorUser)) {

            addRequestToElevator(currentFloor, elevatorUser, listOfWaitingPeopleOnFloor);

            if (!lightUser) {
                System.out.println("Użytkownik " + elevatorUser.getName()
                        + " jest za ciężki aby go przewieść którąkolwiek windą i nie został dodany do kolejki");
            } else if (elevatorUser instanceof Passenger) {
                System.out.println("Osoba o imieniu " + elevatorUser.getName() + " jest na piętrze "
                        + currentFloor.getFloorNo() + " i chce jechać na pietro "
                        + elevatorUser.getWantToGoFloor().getFloorNo());
            } else if (elevatorUser instanceof Cargo) {
                System.out.println("Towar o nazwie " + elevatorUser.getName() + " jest na piętrze "
                        + currentFloor.getFloorNo() + " i chce jechać na pietro "
                        + elevatorUser.getWantToGoFloor().getFloorNo());
            }
        } else {
            System.out.println("Użytkownik " + elevatorUser.getName()
                    + " ma zdefiniowane NIEPRAWIDŁOWE piętro i nie został dodany do kolejki");
        }

    }

    /**
     * Metoda dodaje żądanie (które ma być obsłużone) do odpowiedniej windy.
     *
     * @param currentFloor               - piętro na którym czeka użytkownik
     * @param elevatorUser               - użytkownik
     * @param listOfWaitingPeopleOnFloor - kolejka oczekujących użytkowników
     *                                   na danym piętrze
     */
    private void addRequestToElevator(Floor currentFloor, ElevatorUser elevatorUser,
                                      List<ElevatorUser> listOfWaitingPeopleOnFloor) {

        boolean flag = true;
        int counter = 0;
        int size = elevators.size();

        while (flag && counter < size) {

            Elevator elevator = elevators.pollFirst();//pobieram winde z listy

            if (isUserNotTooHeavy(elevatorUser, elevator)) {

                if ((elevatorUser instanceof Passenger
                        && elevator instanceof PassengerElevator
                        && isCurrentFloorAttended(currentFloor, elevator)
                        && isWantToGoFloorAttended(elevatorUser, elevator))

                        || (elevatorUser instanceof Cargo
                        && elevator instanceof ServiceElevator
                        && isCurrentFloorAttended(currentFloor, elevator)
                        && isWantToGoFloorAttended(elevatorUser, elevator))) {

                    listOfWaitingPeopleOnFloor.add(elevatorUser);

                    Deque<Floor> kolejka = elevator.getRequestedFloors();
                    kolejka.add(currentFloor);
                    elevator.setRequestedFloors(kolejka);//dodaje request do kolejki
                    flag = false;
                }

            }
            elevators.add(elevator);
            counter++;
        }
    }

    /**
     * Metoda sprawdza czy piętro na którym znajduje się użytkownik jest
     * obsługiwane przez daną winde.
     *
     * @param currentFloor - piętro na którym jest użytkownik
     * @param elevator     - użytkownik
     * @return {@true} - gdy piętro jest obsługiwane przez winde,
     * {@false} - gdy piętro nie jest obsługiwane
     */
    private boolean isCurrentFloorAttended(Floor currentFloor, Elevator elevator) {
        return elevator.getAvailableFloors().stream()
                .anyMatch(floor -> floor.equals(currentFloor));
    }

    /**
     * Metoda sprawdza czy piętro na którym chce jechać użytkownik jest
     * obsługiwane przez daną winde.
     *
     * @param elevatorUser - użytkownik
     * @param elevator     - winda
     * @return {@true} - gdy piętro jest obsługiwane przez winde,
     * {@false} - gdy piętro nie jest obsługiwane
     */
    private boolean isWantToGoFloorAttended(ElevatorUser elevatorUser, Elevator elevator) {
        Floor wantToGoFloor = elevatorUser.getWantToGoFloor();
        return elevator.getAvailableFloors().stream()
                .anyMatch(floor -> floor.equals(wantToGoFloor));
    }

    /**
     * Metoda zwraca informacje czy masa użytkownika jest mniejsza od udźwigu windy.
     * (Jeśli masa użytkownika jest większa od udźwigu windy to użytkownik nie
     * może być przewieziony i nie zostanie dodany do kolejki na piętrze).
     *
     * @param elevatorUser - użytkownik
     * @param elevator     - winda
     * @return {@true} - masa użytkownika jest mniejsza od udźwigu windy
     * {@false} - masa użytkownika jest większa od udźwigu windy
     */
    private boolean isUserNotTooHeavy(ElevatorUser elevatorUser, Elevator elevator) {
        boolean flag = elevatorUser.getWeight().getAmount() < elevator.getMaxLoad().getAmount();
        isUserHeavy(flag);
        return flag;
    }

    /**
     * Metoda sprawdza czy użytkownik jest na piętrze które znajduje się w budynku oraz
     * czy piętro na które chce jechać znajduje się w budynku.
     *
     * @param elevatorUser - użytkownik
     * @return {@true} - gdy piętra użytkownika znajdują się w budynku (użytkownik będzie
     * dodany do kolejki na danym piętrze),
     * {@false} - gdy któreś z pięter użytkownika jest poza zakresem pięter budynku (użytkownik
     * nie będzie dodany do kolejki)
     */
    private boolean isFloorInBuilding(ElevatorUser elevatorUser) {
        int size = floors.size();

        Floor userCurrentFloor = elevatorUser.getCurrentFloor();
        Floor userWantToGoFloor = elevatorUser.getWantToGoFloor();

        //użytkownik jest na wyższym piętrze niż najwyższe w budynku
        if (userCurrentFloor.getFloorNo() > floors.get(size - 1).getFloorNo()
                //użytkownik jest na niższym piętrze niż najniższe w budynku
                || userCurrentFloor.getFloorNo() < floors.get(0).getFloorNo()
                //użytkownik chce jechać na wyższe piętro niż najwyższe w budynku
                || userWantToGoFloor.getFloorNo() > floors.get(size - 1).getFloorNo()
                //użytkownik chce jechać na niższe piętro niż najniższe w budynku
                || userWantToGoFloor.getFloorNo() < floors.get(0).getFloorNo()
                //użytkownik ma to samo piętro na którym jest i na które chce jechać
                || userCurrentFloor.getFloorNo() == userWantToGoFloor.getFloorNo()) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Metoda ma za zadanie zwrócić informację, czy na piętrze zadanym parametrem
     * {@code floor} ktos oczekuję.
     *
     * @param floor - piętro na którym sprawdzamy kolejkę
     * @return {@code true} jeżeli czeka jakakolwiek osoba na tym piętrze, {
     * @code false} w przeciwnym razie
     */
    @Override
    public boolean isAnyQueueOnFloor(Floor floor) {
        boolean isQueue = false;
        List<ElevatorUser> elevatorUsers = elevatorQueue.get(floor);
        if (elevatorUsers != null && elevatorUsers.size() > 0) {
            isQueue = true;
        }
        return isQueue;
    }

    /**
     * Metoda ma za zadanie zwórcić informację o osobach cozekujących na windę
     * na konkretnym piętrze zadanym parametrem {@code floor}.
     *
     * @param floor - piętro na którym sprawdzamy kolejkę
     * @return Lista osób czekająca na windę, lub {@code null} jeżeli nikt nie oczekuje
     */
    @Override
    public List<ElevatorUser> getQueue(Floor floor) {
        return elevatorQueue.get(floor);
    }

    /**
     * Ma za zadanie usunąć pasażera {@code passenger} z kolejki.
     *
     * @param elevatorUser - użytkownik który ma zostać usunięty
     */
    @Override
    public void removePersonFromQueue(ElevatorUser elevatorUser) {
        List<ElevatorUser> lista = elevatorQueue.get(elevatorUser.getCurrentFloor());
        lista.remove(elevatorUser);
    }
}
