package block;

import address.Address;
import floor.Floor;

import java.util.List;

/**
 * Klasa reprezentująca blok - abstrakcyjna.
 */
public abstract class Block {

    /**
     * Lista pięter w budynku.
     */
    protected List<Floor> floors;


    /**
     * Adres.
     */
    private Address address;

    /**
     * Konstruktor jednoparametrowy.
     *
     * @param floors - lista pięter w bloku
     */
    public Block(List<Floor> floors) {
        this.floors = floors;
    }

    /**
     * Zwraca liste pięter w budynku.
     *
     * @return floors - lista pięter
     */
    public List<Floor> getFloors() {
        return floors;
    }

    /**
     * Ustawia piętra w budynku.
     *
     * @param floors - lista pięter w budynku
     */
    public void setFloors(List<Floor> floors) {
        this.floors = floors;
    }

    /**
     * Zwraca adres budynku.
     *
     * @return address - adres budynku
     */
    public Address getAddress() {
        return address;
    }

    /**
     * Ustawia adres budynku.
     *
     * @param address - adres budynku
     */
    public void setAddress(Address address) {
        this.address = address;
    }

    /**
     * Metoda zwracająca informację czy w bloku jest winda.
     *
     * @return {@code true} - jeżeli jest winda, {@code false} w przeciwnym razie
     */
    public abstract boolean hasElevator();
}
