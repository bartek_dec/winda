package person;

import address.Address;
import weight.Weigth;

/**
 * Klasa reprezentująca osobę.
 */
public class Person {

    /**
     * Imię.
     */
    protected String name;

    /**
     * Nazwisko.
     */
    protected String surname;

    /**
     * Adres.
     */
    protected Address address;

    /**
     * Waga.
     */
    protected Weigth weight;

    /**
     * Konstruktor jednoparametrowy.
     *
     * @param name - imię
     */
    public Person(String name) {
        this.name = name;
    }

    /**
     * Konstruktor dwuparametrowy.
     *
     * @param name    - imię
     * @param surname - nazwisko
     */
    public Person(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    /**
     * Zwraca imię.
     *
     * @return name - imię
     */
    public String getName() {
        return name;
    }

    /**
     * Ustawia imię.
     *
     * @param name - imię
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Zwraca nazwisko.
     *
     * @return surname - nazwisko
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Ustawia nazwisko.
     *
     * @param surname - nazwisko
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * Zwraca adres osoby.
     *
     * @return address - adres
     */
    public Address getAddress() {
        return address;
    }

    /**
     * Ustawia adres osoby.
     *
     * @param address - adres
     */
    public void setAddress(Address address) {
        this.address = address;
    }

    /**
     * Zwraca wage osoby.
     *
     * @return weight - waga
     */
    public Weigth getWeight() {
        return weight;
    }

    /**
     * Ustawia wage osoby.
     *
     * @param weight - waga
     */
    public void setWeight(Weigth weight) {
        this.weight = weight;
    }

    /**
     * Zwraca string z imieniem osoby.
     *
     * @return name - imię
     */
    @Override
    public String toString() {
        return name;
    }

}
