package person;

import elevator.Direction;
import floor.Floor;
import weight.Weigth;

public interface ElevatorUser {
    /**
     * Metoda zwraca informacje czy użytkownik jest w windzie.
     *
     * @return isInElevator - czy jest w windzie
     */
    boolean isInElevator();

    /**
     * Metoda ustawia informacje czy użytkownik jest w windzie.
     *
     * @param inElevator - czy jest w windzie
     * @return this - zwraca ten obiekt z ustawioną wartością pola
     */
    ElevatorUser setInElevator(boolean inElevator);

    /**
     * Zwraca piętro na które chce jechać użytkownik.
     *
     * @return wantToGoFloor - piętro
     */
    Floor getWantToGoFloor();

    /**
     * Ustawia piętro na które chce jechać użytkownik.
     *
     * @param wantToGoFloor - piętro
     * @return this - zwraca ten obiekt z ustawioną wartością pola
     */
    ElevatorUser setWantToGoFloor(Floor wantToGoFloor);

    /**
     * Zwraca piętro na którym jest użytkownik.
     *
     * @return currentFloor - piętro
     */
    Floor getCurrentFloor();

    /**
     * Ustawia piętro na którym jest użytkownik.
     *
     * @param currentFloor - piętro
     * @return this - ten obiekt z ustawioną wartością pola
     */
    ElevatorUser setCurrentFloor(Floor currentFloor);

    /**
     * Metoda ma za zadanie zwrócić informację w jakim kierunku jedzie użytkownik.
     *
     * @return {@code DOWN} - do dołu, {@code UP} - do góry
     */
    Direction getDirection();

    /**
     * Zwraca nazwe użytkownika windy.
     *
     * @return surname - nazwa
     */
    String getName();

    /**
     * Zwraca wage użytkownika windy.
     *
     * @return weight - waga
     */
    Weigth getWeight();
}
