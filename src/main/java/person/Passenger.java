package person;

import elevator.Direction;
import floor.Floor;
import weight.Weigth;

/**
 * Klasa reprezentujaca użytkownika windy.
 */
public class Passenger extends Person implements ElevatorUser {

    /**
     * Przeciętna masa pasażera podana w KG.
     */
    public static final int AVERAGE_WEIGHT_OF_PASSENGER = 90;

    /**
     * Czy znajduje się w windzie.
     */
    private boolean isInElevator = false;

    /**
     * Na które piętro użytkownik chce jechać.
     */
    private Floor wantToGoFloor;

    /**
     * Z którego piętra użytkownik chce jechać.
     */
    private Floor currentFloor;

    /**
     * Konstruktor jednoparametrowy.
     *
     * @param firstname - imię
     */
    public Passenger(String firstname) {
        super(firstname);
    }

    /**
     * Konstruktor trzyparametrowy.
     *
     * @param firstName     - imię
     * @param currentFloor  - piętro na którym się znajduje
     * @param wantToGoFloor - piętro na które chce jechać
     */
    public Passenger(String firstName, Floor currentFloor, Floor wantToGoFloor) {
        super(firstName);
        this.currentFloor = currentFloor;
        this.wantToGoFloor = wantToGoFloor;
    }

    /**
     * Konstruktor czteroparametrowy.
     *
     * @param firstName     - imię
     * @param currentFloor  - piętro na którym się znajduje
     * @param wantToGoFloor - piętro na które chce jechać
     * @param weigth        - waga
     */
    public Passenger(String firstName, Floor currentFloor, Floor wantToGoFloor, Weigth weigth) {
        this(firstName, currentFloor, wantToGoFloor);
        this.weight = weigth;

    }

    /**
     * Metoda zwraca informacje czy użytkownik jest w windzie.
     *
     * @return isInElevator - czy jest w windzie
     */
    @Override
    public boolean isInElevator() {
        return isInElevator;
    }

    /**
     * Metoda ustawia informacje czy użytkownik jest w windzie.
     *
     * @param inElevator - czy jest w windzie
     * @return this - zwraca ten obiekt z ustawioną wartością pola
     */
    @Override
    public Passenger setInElevator(boolean inElevator) {
        isInElevator = inElevator;
        return this;
    }

    /**
     * Zwraca piętro na które chce jechać użytkownik.
     *
     * @return wantToGoFloor - piętro
     */
    @Override
    public Floor getWantToGoFloor() {
        return wantToGoFloor;
    }

    /**
     * Ustawia piętro na które chce jechać użytkownik.
     *
     * @param wantToGoFloor - piętro
     * @return this - zwraca ten obiekt z ustawioną wartością pola
     */
    @Override
    public Passenger setWantToGoFloor(Floor wantToGoFloor) {
        this.wantToGoFloor = wantToGoFloor;
        return this;
    }

    /**
     * Zwraca piętro na którym jest użytkownik.
     *
     * @return currentFloor - piętro
     */
    @Override
    public Floor getCurrentFloor() {
        return currentFloor;
    }

    /**
     * Ustawia piętro na którym jest użytkownik.
     *
     * @param currentFloor - piętro
     * @return this - ten obiekt z ustawioną wartością pola
     */
    @Override
    public Passenger setCurrentFloor(Floor currentFloor) {
        this.currentFloor = currentFloor;
        return this;
    }

    /**
     * Metoda ma za zadanie zwrócić informację w jakim kierunku jedzie użytkownik.
     *
     * @return {@code DOWN} - do dołu, {@code UP} - do góry
     */
    @Override
    public Direction getDirection() {
        Direction result = Direction.DOWN;
        if (wantToGoFloor.getFloorNo() > currentFloor.getFloorNo()) {
            result = Direction.UP;

        }
        return result;
    }
}