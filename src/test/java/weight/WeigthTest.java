package weight;

import org.junit.Test;

import static org.junit.Assert.*;


public class WeigthTest {

    @Test
    public void whenPutting1LBThen1LBIsExpected() {
        Weigth weigth = new Weigth(1.0f, WeightMetrics.LB);
        float result = weigth.getWeight(WeightMetrics.LB);

        assertEquals(1f, result,0);
    }

    @Test
    public void whenPutting45KGThen100LBIsExpected() {
        Weigth weigth = new Weigth(45, WeightMetrics.KG);
        float result = weigth.getWeight(WeightMetrics.LB);

        assertEquals(100f, result, 0);
    }
}