package elevator;

import block.BlockWithElevators;
import cargo.Cargo;
import floor.Floor;
import org.junit.Before;
import org.junit.Test;
import person.ElevatorUser;
import person.Passenger;
import weight.WeightMetrics;
import weight.Weigth;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class ServiceElevatorTest {
    private ElevatorUser user1;
    private ElevatorUser user2;
    private ElevatorUser user3;
    private ElevatorUser user4;
    private Elevator elevatorAllFloors;
    private Elevator elevatorEvenFloors;
    private Elevator elevatorOddFloors;
    private BlockWithElevators blockWithElevators;

    @Before
    public void setUp() {
        ///Inicjalizacja list pięter w budynku i obsługiwanych przez windy
        List<Floor> allFloors = new ArrayList<>(Arrays.asList(new Floor(-1),
                new Floor(0), new Floor(1), new Floor(2), new Floor(3),
                new Floor(4), new Floor(5), new Floor(6), new Floor(7), new Floor(8),
                new Floor(9), new Floor(10)));

        List<Floor> evenFloors = new ArrayList<>(Arrays.asList(new Floor(0), new Floor(2),
                new Floor(4), new Floor(6), new Floor(8), new Floor(10)));

        List<Floor> oddFloors = new ArrayList<>(Arrays.asList(new Floor(-1), new Floor(0),
                new Floor(1), new Floor(3), new Floor(5), new Floor(7), new Floor(9)));

        //Inicjalizacja bloku i dodanie pięter
        blockWithElevators = new BlockWithElevators(allFloors);

        //inicjalizacja wind pasażerskich
        elevatorAllFloors = new ServiceElevator();
        elevatorEvenFloors = new ServiceElevator();
        elevatorOddFloors = new ServiceElevator();

        //dodanie pięter obsługiwanych przez windy
        elevatorAllFloors.setAvailableFloors(allFloors);
        elevatorEvenFloors.setAvailableFloors(evenFloors);
        elevatorOddFloors.setAvailableFloors(oddFloors);

        //dodanie udźwigu wind
        elevatorAllFloors.setMaxLoad(new Weigth(300, WeightMetrics.KG));
        elevatorEvenFloors.setMaxLoad(new Weigth(300, WeightMetrics.KG));
        elevatorOddFloors.setMaxLoad(new Weigth(300, WeightMetrics.KG));

    }

    ///////TESTY sprawdzające kolejność zabierania pasażerów

    @Test
    public void whenElevatorOn0ReceiveRequestOnFloor6ThenDirectionIsUp() {
        Floor currentFloorU1 = new Floor(6);
        Floor wantToGoFloorU1 = new Floor(1);

        user1 = new Cargo("DESK", currentFloorU1, wantToGoFloorU1, new Weigth(70F, WeightMetrics.KG));

        LinkedList<Elevator> listOfElevators = new LinkedList<>();
        listOfElevators.add(elevatorAllFloors);
        blockWithElevators.setElevators(listOfElevators);

        blockWithElevators.addElevatorUserToQueue(currentFloorU1, user1);

        elevatorAllFloors.setCurrentFloor(new Floor(0));
        elevatorAllFloors.nextStep();

        Direction result = Direction.UP;

        assertEquals(result, elevatorAllFloors.getDirection());
    }

    @Test
    public void whenElevatorOn0ReceiveRequestOnFloorNegative1ThenDirectionIsDown() {
        Floor currentFloorU1 = new Floor(-1);
        Floor wantToGoFloorU1 = new Floor(1);

        user1 = new Cargo("DESK", currentFloorU1, wantToGoFloorU1, new Weigth(70F, WeightMetrics.KG));

        LinkedList<Elevator> listOfElevators = new LinkedList<>();
        listOfElevators.add(elevatorAllFloors);
        blockWithElevators.setElevators(listOfElevators);

        blockWithElevators.addElevatorUserToQueue(currentFloorU1, user1);

        elevatorAllFloors.setCurrentFloor(new Floor(0));
        elevatorAllFloors.nextStep();

        Direction result = Direction.DOWN;

        assertEquals(result, elevatorAllFloors.getDirection());
    }

    @Test
    public void whenUser1From6To1ThenUser1ShallBePickedUpOn6() {
        Floor currentFloorU1 = new Floor(6);
        Floor wantToGoFloorU1 = new Floor(1);

        user1 = new Cargo("DESK", currentFloorU1, wantToGoFloorU1, new Weigth(70F, WeightMetrics.KG));

        LinkedList<Elevator> listOfElevators = new LinkedList<>();
        listOfElevators.add(elevatorAllFloors);
        blockWithElevators.setElevators(listOfElevators);

        blockWithElevators.addElevatorUserToQueue(currentFloorU1, user1);

        elevatorAllFloors.setCurrentFloor(new Floor(6));
        elevatorAllFloors.setDirection(Direction.UP);
        elevatorAllFloors.nextStep();

        int usersOnBoardListSize = 1;

        assertEquals(usersOnBoardListSize, elevatorAllFloors.getUsersOnBoard().size());
    }

    @Test
    public void whenUser1From6To1ThenQueueOn6ShallBeEmpty() {
        Floor currentFloorU1 = new Floor(6);
        Floor wantToGoFloorU1 = new Floor(1);

        user1 = new Cargo("DESK", currentFloorU1, wantToGoFloorU1, new Weigth(70F, WeightMetrics.KG));

        LinkedList<Elevator> listOfElevators = new LinkedList<>();
        listOfElevators.add(elevatorAllFloors);
        blockWithElevators.setElevators(listOfElevators);

        blockWithElevators.addElevatorUserToQueue(currentFloorU1, user1);

        elevatorAllFloors.setCurrentFloor(new Floor(6));
        elevatorAllFloors.setDirection(Direction.UP);
        elevatorAllFloors.nextStep();

        int queueSize = 0;

        assertEquals(queueSize, blockWithElevators.getQueue(currentFloorU1).size());
    }

    @Test
    public void whenUser1From6To1ThenUser1ShallBeLeftOn1() {
        Floor currentFloorU1 = new Floor(6);
        Floor wantToGoFloorU1 = new Floor(1);

        user1 = new Cargo("DESK", currentFloorU1, wantToGoFloorU1, new Weigth(70F, WeightMetrics.KG));

        LinkedList<Elevator> listOfElevators = new LinkedList<>();
        listOfElevators.add(elevatorAllFloors);
        blockWithElevators.setElevators(listOfElevators);

        blockWithElevators.addElevatorUserToQueue(currentFloorU1, user1);

        elevatorAllFloors.setCurrentFloor(new Floor(1));
        elevatorAllFloors.setDirection(Direction.UP);
        elevatorAllFloors.nextStep();

        int usersOnBoardListSize = 0;

        assertEquals(usersOnBoardListSize, elevatorAllFloors.getUsersOnBoard().size());
    }

    @Test
    public void whenUser1From6To1AndUser2From4To1ThenUser2ShallBeSkippedOn4() {
        Floor currentFloorU1 = new Floor(6);
        Floor wantToGoFloorU1 = new Floor(1);

        Floor currentFloorU2 = new Floor(4);
        Floor wantToGoFloorU2 = new Floor(1);

        user1 = new Cargo("DESK", currentFloorU1, wantToGoFloorU1, new Weigth(70F, WeightMetrics.KG));
        user2 = new Cargo("BED", currentFloorU2, wantToGoFloorU2, new Weigth(65F, WeightMetrics.KG));

        LinkedList<Elevator> listOfElevators = new LinkedList<>();
        listOfElevators.add(elevatorAllFloors);
        blockWithElevators.setElevators(listOfElevators);

        blockWithElevators.addElevatorUserToQueue(currentFloorU1, user1);
        blockWithElevators.addElevatorUserToQueue(currentFloorU2, user2);

        elevatorAllFloors.setCurrentFloor(new Floor(4));
        elevatorAllFloors.setDirection(Direction.UP);
        elevatorAllFloors.nextStep();

        int usersOnBoardListSize = 0;

        assertEquals(usersOnBoardListSize, elevatorAllFloors.getUsersOnBoard().size());
    }

    @Test
    public void whenUser1From1To6AndUser2From3To5ThenUser2ShallBePickedUp() {
        Floor currentFloorU1 = new Floor(1);
        Floor wantToGoFloorU1 = new Floor(6);

        Floor currentFloorU2 = new Floor(3);
        Floor wantToGoFloorU2 = new Floor(5);

        user1 = new Cargo("DESK", currentFloorU1, wantToGoFloorU1, new Weigth(70F, WeightMetrics.KG));
        user2 = new Cargo("BED", currentFloorU2, wantToGoFloorU2, new Weigth(65F, WeightMetrics.KG));

        LinkedList<Elevator> listOfElevators = new LinkedList<>();
        listOfElevators.add(elevatorAllFloors);
        blockWithElevators.setElevators(listOfElevators);

        List<ElevatorUser> list = new ArrayList<>(Arrays.asList(user1));
        elevatorAllFloors.setUsersOnBoard(list);
        blockWithElevators.addElevatorUserToQueue(currentFloorU2, user2);

        elevatorAllFloors.setCurrentFloor(new Floor(3));
        elevatorAllFloors.setDirection(Direction.UP);
        elevatorAllFloors.nextStep();

        String resultName = "BED";

        assertEquals(resultName, elevatorAllFloors.getUsersOnBoard().get(1).getName());
    }

    @Test
    public void whenUser1From1To6AndUser2From3To5ThenUser2ShallBeLeftOn5() {
        Floor currentFloorU1 = new Floor(1);
        Floor wantToGoFloorU1 = new Floor(6);

        Floor currentFloorU2 = new Floor(3);
        Floor wantToGoFloorU2 = new Floor(5);

        user1 = new Cargo("DESK", currentFloorU1, wantToGoFloorU1, new Weigth(70F, WeightMetrics.KG));
        user2 = new Cargo("BED", currentFloorU2, wantToGoFloorU2, new Weigth(65F, WeightMetrics.KG));

        LinkedList<Elevator> listOfElevators = new LinkedList<>();
        listOfElevators.add(elevatorAllFloors);
        blockWithElevators.setElevators(listOfElevators);

        List<ElevatorUser> usersOnBoard = new ArrayList<>(Arrays.asList(user1, user2));
        elevatorAllFloors.setUsersOnBoard(usersOnBoard);
        elevatorAllFloors.setCurrentFloor(new Floor(5));
        elevatorAllFloors.setDirection(Direction.UP);
        elevatorAllFloors.nextStep();

        int usersOnBoardListSize = 1;
        String resultName = "DESK";

        assertEquals(usersOnBoardListSize, elevatorAllFloors.getUsersOnBoard().size());
        assertEquals(resultName, elevatorAllFloors.getUsersOnBoard().get(0).getName());

    }

    @Test
    public void whenUser1From1To5AndUser2From3To8ThenUser1ShallBeLeftOn5() {
        Floor currentFloorU1 = new Floor(1);
        Floor wantToGoFloorU1 = new Floor(5);

        Floor currentFloorU2 = new Floor(3);
        Floor wantToGoFloorU2 = new Floor(8);

        user1 = new Cargo("DESK", currentFloorU1, wantToGoFloorU1, new Weigth(70F, WeightMetrics.KG));
        user2 = new Cargo("BED", currentFloorU2, wantToGoFloorU2, new Weigth(65F, WeightMetrics.KG));

        LinkedList<Elevator> listOfElevators = new LinkedList<>();
        listOfElevators.add(elevatorAllFloors);
        blockWithElevators.setElevators(listOfElevators);

        List<ElevatorUser> usersOnBoard = new ArrayList<>(Arrays.asList(user1, user2));
        elevatorAllFloors.setUsersOnBoard(usersOnBoard);
        elevatorAllFloors.setCurrentFloor(new Floor(5));
        elevatorAllFloors.setDirection(Direction.UP);
        elevatorAllFloors.nextStep();

        int usersOnBoardListSize = 1;
        String resultName = "BED";

        assertEquals(usersOnBoardListSize, elevatorAllFloors.getUsersOnBoard().size());
        assertEquals(resultName, elevatorAllFloors.getUsersOnBoard().get(0).getName());

    }

    @Test
    public void whenUser1From5To8AndUser2From2To3ThenUser2ShallBePickedUpOn2() {
        Floor currentFloorU1 = new Floor(5);
        Floor wantToGoFloorU1 = new Floor(8);

        Floor currentFloorU2 = new Floor(2);
        Floor wantToGoFloorU2 = new Floor(3);

        user1 = new Cargo("DESK", currentFloorU1, wantToGoFloorU1, new Weigth(70F, WeightMetrics.KG));
        user2 = new Cargo("BED", currentFloorU2, wantToGoFloorU2, new Weigth(65F, WeightMetrics.KG));

        LinkedList<Elevator> listOfElevators = new LinkedList<>();
        listOfElevators.add(elevatorAllFloors);
        blockWithElevators.setElevators(listOfElevators);

        blockWithElevators.addElevatorUserToQueue(currentFloorU1, user1);
        blockWithElevators.addElevatorUserToQueue(currentFloorU2, user2);

        elevatorAllFloors.setCurrentFloor(new Floor(2));
        elevatorAllFloors.setDirection(Direction.UP);
        elevatorAllFloors.nextStep();

        int usersOnBoardListSize = 1;
        String resultName = "BED";

        assertEquals(usersOnBoardListSize, elevatorAllFloors.getUsersOnBoard().size());
        assertEquals(resultName, elevatorAllFloors.getUsersOnBoard().get(0).getName());

    }

    @Test
    public void whenUser1From5To8AndUser2From2To3ThenUser2ShallBeLeftOn3() {
        Floor currentFloorU1 = new Floor(5);
        Floor wantToGoFloorU1 = new Floor(8);

        Floor currentFloorU2 = new Floor(2);
        Floor wantToGoFloorU2 = new Floor(3);

        user1 = new Cargo("DESK", currentFloorU1, wantToGoFloorU1, new Weigth(70F, WeightMetrics.KG));
        user2 = new Cargo("BED", currentFloorU2, wantToGoFloorU2, new Weigth(65F, WeightMetrics.KG));

        LinkedList<Elevator> listOfElevators = new LinkedList<>();
        listOfElevators.add(elevatorAllFloors);
        blockWithElevators.setElevators(listOfElevators);

        List<ElevatorUser> usersOnBoard = new ArrayList<>(Arrays.asList(user1, user2));
        elevatorAllFloors.setUsersOnBoard(usersOnBoard);
        elevatorAllFloors.setCurrentFloor(new Floor(3));
        elevatorAllFloors.setDirection(Direction.UP);
        elevatorAllFloors.nextStep();

        int usersOnBoardListSize = 1;
        String resultName = "DESK";

        assertEquals(usersOnBoardListSize, elevatorAllFloors.getUsersOnBoard().size());
        assertEquals(resultName, elevatorAllFloors.getUsersOnBoard().get(0).getName());

    }

    @Test
    public void whenUser1From6To4AndUser2From4To6ThenUser2ShallBePickedUpOn4() {
        Floor currentFloorU1 = new Floor(6);
        Floor wantToGoFloorU1 = new Floor(4);

        Floor currentFloorU2 = new Floor(4);
        Floor wantToGoFloorU2 = new Floor(6);

        user1 = new Cargo("DESK", currentFloorU1, wantToGoFloorU1, new Weigth(70F, WeightMetrics.KG));
        user2 = new Cargo("BED", currentFloorU2, wantToGoFloorU2, new Weigth(65F, WeightMetrics.KG));

        LinkedList<Elevator> listOfElevators = new LinkedList<>();
        listOfElevators.add(elevatorAllFloors);
        blockWithElevators.setElevators(listOfElevators);

        blockWithElevators.addElevatorUserToQueue(currentFloorU1, user1);
        blockWithElevators.addElevatorUserToQueue(currentFloorU2, user2);

        elevatorAllFloors.setCurrentFloor(new Floor(4));
        elevatorAllFloors.setDirection(Direction.UP);
        elevatorAllFloors.nextStep();

        int usersOnBoardListSize = 1;
        String resultName = "BED";

        assertEquals(usersOnBoardListSize, elevatorAllFloors.getUsersOnBoard().size());
        assertEquals(resultName, elevatorAllFloors.getUsersOnBoard().get(0).getName());

    }

    @Test
    public void whenUser1From6To8AndUser2From4To8ThenUser2ShallBePickedUpOn4() {
        Floor currentFloorU1 = new Floor(6);
        Floor wantToGoFloorU1 = new Floor(8);

        Floor currentFloorU2 = new Floor(4);
        Floor wantToGoFloorU2 = new Floor(8);

        user1 = new Cargo("DESK", currentFloorU1, wantToGoFloorU1, new Weigth(70F, WeightMetrics.KG));
        user2 = new Cargo("BED", currentFloorU2, wantToGoFloorU2, new Weigth(65F, WeightMetrics.KG));

        LinkedList<Elevator> listOfElevators = new LinkedList<>();
        listOfElevators.add(elevatorAllFloors);
        blockWithElevators.setElevators(listOfElevators);

        blockWithElevators.addElevatorUserToQueue(currentFloorU1, user1);
        blockWithElevators.addElevatorUserToQueue(currentFloorU2, user2);

        elevatorAllFloors.setCurrentFloor(new Floor(4));
        elevatorAllFloors.setDirection(Direction.UP);
        elevatorAllFloors.nextStep();

        int usersOnBoardListSize = 1;
        String resultName = "BED";

        assertEquals(usersOnBoardListSize, elevatorAllFloors.getUsersOnBoard().size());
        assertEquals(resultName, elevatorAllFloors.getUsersOnBoard().get(0).getName());

    }

    @Test
    public void whenUser1From6To4AndUser2From4To8ThenUser2ShallBeSkippedOn4() {
        Floor currentFloorU1 = new Floor(6);
        Floor wantToGoFloorU1 = new Floor(4);

        Floor currentFloorU2 = new Floor(4);
        Floor wantToGoFloorU2 = new Floor(8);

        user1 = new Cargo("DESK", currentFloorU1, wantToGoFloorU1, new Weigth(70F, WeightMetrics.KG));
        user2 = new Cargo("BED", currentFloorU2, wantToGoFloorU2, new Weigth(65F, WeightMetrics.KG));

        LinkedList<Elevator> listOfElevators = new LinkedList<>();
        listOfElevators.add(elevatorAllFloors);
        blockWithElevators.setElevators(listOfElevators);

        blockWithElevators.addElevatorUserToQueue(currentFloorU1, user1);
        blockWithElevators.addElevatorUserToQueue(currentFloorU2, user2);

        elevatorAllFloors.setCurrentFloor(new Floor(4));
        elevatorAllFloors.setDirection(Direction.UP);
        elevatorAllFloors.nextStep();

        int usersOnBoardListSize = 0;

        assertEquals(usersOnBoardListSize, elevatorAllFloors.getUsersOnBoard().size());
    }

    @Test
    public void whenUser1From3To7AndUser2From2To8ThenUser2ShallBePickedOn2() {
        Floor currentFloorU1 = new Floor(3);
        Floor wantToGoFloorU1 = new Floor(7);

        Floor currentFloorU2 = new Floor(2);
        Floor wantToGoFloorU2 = new Floor(8);

        user1 = new Cargo("DESK", currentFloorU1, wantToGoFloorU1, new Weigth(70F, WeightMetrics.KG));
        user2 = new Cargo("BED", currentFloorU2, wantToGoFloorU2, new Weigth(65F, WeightMetrics.KG));

        LinkedList<Elevator> listOfElevators = new LinkedList<>();
        listOfElevators.add(elevatorAllFloors);
        blockWithElevators.setElevators(listOfElevators);

        blockWithElevators.addElevatorUserToQueue(currentFloorU1, user1);
        blockWithElevators.addElevatorUserToQueue(currentFloorU2, user2);

        elevatorAllFloors.setCurrentFloor(new Floor(2));
        elevatorAllFloors.setDirection(Direction.UP);
        elevatorAllFloors.nextStep();

        int usersOnBoardListSize = 1;

        assertEquals(usersOnBoardListSize, elevatorAllFloors.getUsersOnBoard().size());
    }

    @Test
    public void whenUserWantToGoOnFloorNegative2ThenQueueEmpty() {
        Floor currentFloorU1 = new Floor(6);
        Floor wantToGoFloorU1 = new Floor(-2);

        user1 = new Cargo("DESK", currentFloorU1, wantToGoFloorU1, new Weigth(70F, WeightMetrics.KG));

        LinkedList<Elevator> listOfElevators = new LinkedList<>();
        listOfElevators.add(elevatorAllFloors);
        blockWithElevators.setElevators(listOfElevators);

        blockWithElevators.addElevatorUserToQueue(currentFloorU1, user1);

        int usersInQueue = 0;

        assertEquals(usersInQueue, elevatorAllFloors.getRequestedFloors().size());
    }

    @Test
    public void whenUserWantToGoOnFloor12ThenQueueEmpty() {
        Floor currentFloorU1 = new Floor(6);
        Floor wantToGoFloorU1 = new Floor(12);

        user1 = new Cargo("DESK", currentFloorU1, wantToGoFloorU1, new Weigth(70F, WeightMetrics.KG));

        LinkedList<Elevator> listOfElevators = new LinkedList<>();
        listOfElevators.add(elevatorAllFloors);
        blockWithElevators.setElevators(listOfElevators);

        blockWithElevators.addElevatorUserToQueue(currentFloorU1, user1);

        int usersInQueue = 0;

        assertEquals(usersInQueue, elevatorAllFloors.getRequestedFloors().size());
    }

    @Test
    public void whenUserCurrentFloorNegative2ThenQueueEmpty() {
        Floor currentFloorU1 = new Floor(-2);
        Floor wantToGoFloorU1 = new Floor(6);

        user1 = new Cargo("DESK", currentFloorU1, wantToGoFloorU1, new Weigth(70F, WeightMetrics.KG));

        LinkedList<Elevator> listOfElevators = new LinkedList<>();
        listOfElevators.add(elevatorAllFloors);
        blockWithElevators.setElevators(listOfElevators);

        blockWithElevators.addElevatorUserToQueue(currentFloorU1, user1);

        int usersInQueue = 0;

        assertEquals(usersInQueue, elevatorAllFloors.getRequestedFloors().size());
    }

    @Test
    public void whenUserCurrentFloor12ThenQueueEmpty() {
        Floor currentFloorU1 = new Floor(12);
        Floor wantToGoFloorU1 = new Floor(6);

        user1 = new Cargo("DESK", currentFloorU1, wantToGoFloorU1, new Weigth(70F, WeightMetrics.KG));

        LinkedList<Elevator> listOfElevators = new LinkedList<>();
        listOfElevators.add(elevatorAllFloors);
        blockWithElevators.setElevators(listOfElevators);

        blockWithElevators.addElevatorUserToQueue(currentFloorU1, user1);

        int usersInQueue = 0;

        assertEquals(usersInQueue, elevatorAllFloors.getRequestedFloors().size());
    }

    @Test
    public void whenUserCurrentFloorIs2AndWantToGoOnFloor2ThenQueueEmpty() {
        Floor currentFloorU1 = new Floor(2);
        Floor wantToGoFloorU1 = new Floor(2);

        user1 = new Cargo("DESK", currentFloorU1, wantToGoFloorU1, new Weigth(70F, WeightMetrics.KG));

        LinkedList<Elevator> listOfElevators = new LinkedList<>();
        listOfElevators.add(elevatorAllFloors);
        blockWithElevators.setElevators(listOfElevators);

        blockWithElevators.addElevatorUserToQueue(currentFloorU1, user1);

        int usersInQueue = 0;

        assertEquals(usersInQueue, elevatorAllFloors.getRequestedFloors().size());
    }
    @Test
    public void whenPassengerWantToEnterServiceElevatorThenItIsNotAccepted() {
        Floor currentFloorU1 = new Floor(3);
        Floor wantToGoFloorU1 = new Floor(7);

        user1 = new Passenger("TOM", currentFloorU1, wantToGoFloorU1, new Weigth(70F, WeightMetrics.KG));


        LinkedList<Elevator> listOfElevators = new LinkedList<>();
        listOfElevators.add(elevatorAllFloors);
        blockWithElevators.setElevators(listOfElevators);

        blockWithElevators.addElevatorUserToQueue(currentFloorU1, user1);

        elevatorAllFloors.setCurrentFloor(new Floor(3));
        elevatorAllFloors.setDirection(Direction.UP);
        elevatorAllFloors.nextStep();

        int usersOnBoardListSize = 0;

        assertEquals(usersOnBoardListSize, elevatorAllFloors.getUsersOnBoard().size());
    }

    ////////TESTY dla dwóch wind

    @Test
    public void whenUserCurrentFloorIs2AndWantToGoOnFloor3ThenElevatorOddFloorsRequestedFloorSizeIs0() {
        Floor currentFloorU1 = new Floor(2);
        Floor wantToGoFloorU1 = new Floor(3);

        user1 = new Cargo("DESK", currentFloorU1, wantToGoFloorU1, new Weigth(70F, WeightMetrics.KG));

        LinkedList<Elevator> listOfElevators = new LinkedList<>();
        listOfElevators.add(elevatorOddFloors);
        listOfElevators.add(elevatorAllFloors);
        blockWithElevators.setElevators(listOfElevators);

        blockWithElevators.addElevatorUserToQueue(currentFloorU1, user1);

        int requestedFloorSize = 0;

        assertEquals(requestedFloorSize, elevatorOddFloors.getRequestedFloors().size());
    }

    @Test
    public void whenUserCurrentFloorIs2AndWantToGoOnFloor3ThenElevatorAllFloorsRequestedFloorSizeIs1() {
        Floor currentFloorU1 = new Floor(2);
        Floor wantToGoFloorU1 = new Floor(3);

        user1 = new Cargo("DESK", currentFloorU1, wantToGoFloorU1, new Weigth(70F, WeightMetrics.KG));

        LinkedList<Elevator> listOfElevators = new LinkedList<>();
        listOfElevators.add(elevatorOddFloors);
        listOfElevators.add(elevatorAllFloors);
        blockWithElevators.setElevators(listOfElevators);

        blockWithElevators.addElevatorUserToQueue(currentFloorU1, user1);

        int requestedFloorSize = 1;

        assertEquals(requestedFloorSize, elevatorAllFloors.getRequestedFloors().size());
    }

    @Test
    public void whenUserCurrentFloorIs2AndWantToGoOnFloor5ThenElevatorEvenFloorsRequestedFloorSizeIs0() {
        Floor currentFloorU1 = new Floor(2);
        Floor wantToGoFloorU1 = new Floor(5);

        user1 = new Cargo("DESK", currentFloorU1, wantToGoFloorU1, new Weigth(70F, WeightMetrics.KG));

        LinkedList<Elevator> listOfElevators = new LinkedList<>();
        listOfElevators.add(elevatorEvenFloors);
        listOfElevators.add(elevatorAllFloors);
        blockWithElevators.setElevators(listOfElevators);

        blockWithElevators.addElevatorUserToQueue(currentFloorU1, user1);

        int requestedFloorSize = 0;

        assertEquals(requestedFloorSize, elevatorEvenFloors.getRequestedFloors().size());
    }

    @Test
    public void whenUserCurrentFloorIs2AndWantToGoOnFloor5ThenElevatorAllFloorsRequestedFloorSizeIs1() {
        Floor currentFloorU1 = new Floor(2);
        Floor wantToGoFloorU1 = new Floor(5);

        user1 = new Cargo("DESK", currentFloorU1, wantToGoFloorU1, new Weigth(70F, WeightMetrics.KG));

        LinkedList<Elevator> listOfElevators = new LinkedList<>();
        listOfElevators.add(elevatorEvenFloors);
        listOfElevators.add(elevatorAllFloors);
        blockWithElevators.setElevators(listOfElevators);

        blockWithElevators.addElevatorUserToQueue(currentFloorU1, user1);

        int requestedFloorSize = 1;

        assertEquals(requestedFloorSize, elevatorAllFloors.getRequestedFloors().size());
    }

    @Test
    public void whenUserCurrentFloorIs2AndWantToGoOnFloor6ThenElevatorEvenFloorsRequestedFloorSizeIs1() {
        Floor currentFloorU1 = new Floor(2);
        Floor wantToGoFloorU1 = new Floor(6);

        user1 = new Cargo("DESK", currentFloorU1, wantToGoFloorU1, new Weigth(70F, WeightMetrics.KG));

        LinkedList<Elevator> listOfElevators = new LinkedList<>();
        listOfElevators.add(elevatorOddFloors);
        listOfElevators.add(elevatorEvenFloors);
        blockWithElevators.setElevators(listOfElevators);

        blockWithElevators.addElevatorUserToQueue(currentFloorU1, user1);

        int requestedFloorSize = 1;

        assertEquals(requestedFloorSize, elevatorEvenFloors.getRequestedFloors().size());
    }

    @Test
    public void whenUserCurrentFloorIs3AndWantToGoOnFloor7ThenElevatorOddFloorsRequestedFloorSizeIs1() {
        Floor currentFloorU1 = new Floor(3);
        Floor wantToGoFloorU1 = new Floor(7);

        user1 = new Cargo("DESK", currentFloorU1, wantToGoFloorU1, new Weigth(70F, WeightMetrics.KG));

        LinkedList<Elevator> listOfElevators = new LinkedList<>();
        listOfElevators.add(elevatorEvenFloors);
        listOfElevators.add(elevatorOddFloors);
        blockWithElevators.setElevators(listOfElevators);

        blockWithElevators.addElevatorUserToQueue(currentFloorU1, user1);

        int requestedFloorSize = 1;

        assertEquals(requestedFloorSize, elevatorOddFloors.getRequestedFloors().size());
    }

    ////////TESTY sprawdzające tonaż windy

    @Test
    public void whenMaxLoad300ThenSafeLoadIs200() {
        float result = 200.0F;

        assertEquals(result, elevatorAllFloors.getMaxLoad().getAmount(), 0.01F);
    }

    @Test
    public void whenMaxLoad300ThenMaxNumberOfPassengersIs2() {
        short result = 2;
        assertEquals(result, elevatorAllFloors.getMaxNumberOfUsers());
    }

    @Test
    public void when4LightPassengersWaitingOnTheFloorThenOnly2PickedUp() {
        Floor currentFloorU1 = new Floor(1);
        Floor wantToGoFloorU1 = new Floor(2);

        Floor currentFloorU2 = new Floor(1);
        Floor wantToGoFloorU2 = new Floor(2);

        Floor currentFloorU3 = new Floor(1);
        Floor wantToGoFloorU3 = new Floor(2);

        Floor currentFloorU4 = new Floor(1);
        Floor wantToGoFloorU4 = new Floor(2);

        user1 = new Cargo("BED", currentFloorU1, wantToGoFloorU1, new Weigth(50F, WeightMetrics.KG));
        user2 = new Cargo("DESK", currentFloorU2, wantToGoFloorU2, new Weigth(50F, WeightMetrics.KG));
        user3 = new Cargo("FRIDGE", currentFloorU3, wantToGoFloorU3, new Weigth(50F, WeightMetrics.KG));
        user4 = new Cargo("TV", currentFloorU4, wantToGoFloorU4, new Weigth(50F, WeightMetrics.KG));

        LinkedList<Elevator> listOfElevators = new LinkedList<>();
        listOfElevators.add(elevatorAllFloors);
        blockWithElevators.setElevators(listOfElevators);

        blockWithElevators.addElevatorUserToQueue(currentFloorU1, user1);
        blockWithElevators.addElevatorUserToQueue(currentFloorU2, user2);
        blockWithElevators.addElevatorUserToQueue(currentFloorU2, user3);
        blockWithElevators.addElevatorUserToQueue(currentFloorU2, user4);

        elevatorAllFloors.setCurrentFloor(new Floor(1));
        elevatorAllFloors.setDirection(Direction.UP);
        elevatorAllFloors.nextStep();

        int usersOnBoard = 2;

        assertEquals(usersOnBoard, elevatorAllFloors.getUsersOnBoard().size());
    }

    @Test
    public void when3HeavyPassengersWaitingOnTheFloorThenOnly2PickedUp() {
        Floor currentFloorU1 = new Floor(1);
        Floor wantToGoFloorU1 = new Floor(2);

        Floor currentFloorU2 = new Floor(1);
        Floor wantToGoFloorU2 = new Floor(2);

        Floor currentFloorU3 = new Floor(1);
        Floor wantToGoFloorU3 = new Floor(2);

        user1 = new Cargo("DESK", currentFloorU1, wantToGoFloorU1, new Weigth(100F, WeightMetrics.KG));
        user2 = new Cargo("BED", currentFloorU2, wantToGoFloorU2, new Weigth(90F, WeightMetrics.KG));
        user3 = new Cargo("FRIDGE", currentFloorU3, wantToGoFloorU3, new Weigth(80F, WeightMetrics.KG));

        LinkedList<Elevator> listOfElevators = new LinkedList<>();
        listOfElevators.add(elevatorAllFloors);
        blockWithElevators.setElevators(listOfElevators);

        blockWithElevators.addElevatorUserToQueue(currentFloorU1, user1);
        blockWithElevators.addElevatorUserToQueue(currentFloorU2, user2);
        blockWithElevators.addElevatorUserToQueue(currentFloorU2, user3);

        elevatorAllFloors.setCurrentFloor(new Floor(1));
        elevatorAllFloors.setDirection(Direction.UP);
        elevatorAllFloors.nextStep();

        int usersOnBoard = 2;

        assertEquals(usersOnBoard, elevatorAllFloors.getUsersOnBoard().size());
    }
    @Test
    public void whenUserIs300KgHeavyThenIsNotAddedToQueue() {
        Floor currentFloorU1 = new Floor(3);
        Floor wantToGoFloorU1 = new Floor(7);

        user1 = new Cargo("FRIDGE", currentFloorU1, wantToGoFloorU1, new Weigth(300F, WeightMetrics.KG));

        LinkedList<Elevator> listOfElevators = new LinkedList<>();
        listOfElevators.add(elevatorAllFloors);
        blockWithElevators.setElevators(listOfElevators);

        blockWithElevators.addElevatorUserToQueue(currentFloorU1, user1);

        assertFalse(blockWithElevators.isAnyQueueOnFloor(currentFloorU1));
    }

}