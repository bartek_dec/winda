package address;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class StreetNameTest {

    @Test
    public void whenCallinTOMASZA_ZANAThenItsStringRepresentationIsExpected() {
        String result = StreetName.TOMASZA_ZANA.getStreetName();

        assertThat(result, is("Tomasza Zana"));
    }

    @Test
    public void whenCallingToStringMethodForPANA_TADEUSZAThenItsStringRepresentationIsExpected() {
        String result = StreetName.PANA_TADEUSZA.toString();
        assertThat(result, is("Pana Tadeusza"));
    }

}