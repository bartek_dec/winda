package address;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class AddressTest {

    Address address1;
    Address address2;

    @Before
    public void setUp() {
        address1 = new Address(City.LUBLIN, StreetName.ALEJE_RACŁAWICKIE);
        address1.setBlockNo(2).setAppartmentNo(13).setPostCode("00-007");

        address2 = new Address(City.LUBLIN, StreetName.ALEJE_RACŁAWICKIE);
        address2.setBlockNo(2).setAppartmentNo(13).setPostCode("00-007");
    }

    @Test
    public void whenCallingGetCityThenLublinIsExpected() {
        City result = address1.getCity();
        assertEquals(City.LUBLIN, result);
    }

    @Test
    public void whenCallingGetCityNameThenALEJE_RACŁAWICKIEIsExpected() {
        StreetName result = address1.getStreetName();
        assertEquals(StreetName.ALEJE_RACŁAWICKIE, result);
    }

    @Test
    public void whenCallingGetBlockNoThen2IsExpected() {
        int result = address1.getBlockNo();
        assertEquals(2, result);
    }

    @Test
    public void whenCallingGetAppartmentNoThen13IsExpected() {
        int result = address1.getAppartmentNo();
        assertEquals(13, result);
    }

    @Test
    public void whenCallingGetPostCodeThen00dash007StringIsExpected() {
        String result = address1.getPostCode();
        assertThat(result, is("00-007"));
    }

    @Test
    public void whenCallingToStringTheStringRepresentationOfAddressIsExpected() {
        String result = address1.toString();
        assertThat(result,is("Lublin ul. Aleje Racławickie 2"));
    }

    @Test
    public void whenSettingWARSZAWAThenItIsExpected() {
        address2.setCity(City.WARSZAWA);
        assertEquals(City.WARSZAWA, address2.getCity());
    }

    @Test
    public void whenSettingALEJE_JEROZOLIMSKIEThenItIsExpected() {
        address2.setStreetName(StreetName.ALEJE_JEROZOLIMSKIE);
        assertEquals(StreetName.ALEJE_JEROZOLIMSKIE, address2.getStreetName());
    }
}