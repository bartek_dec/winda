package address.utils;

import org.junit.Test;

import static org.junit.Assert.*;

public class PostCodeUtilsTest {

    @Test
    public void whenPutting12Dash345AndPOLThenTrueIsExpected() {
        boolean result = PostCodeUtils.isCorrect("12-345", "POL");
        assertTrue(result);
    }

    @Test
    public void whenPutting12Dash345AndNullInPolandThenTrueIsExpected() {
        boolean result = PostCodeUtils.isCorrect("12-345", null);
        assertTrue(result);
    }

    @Test
    public void whenPutting12345AndUSAThenTrueIsExpected() {
        boolean result = PostCodeUtils.isCorrect("12345", "USA");
        assertTrue(result);
    }

    @Test
    public void whenPutting1234SpaceABAndNLDThenTrueIsExpected() {
        boolean result = PostCodeUtils.isCorrect("1234 AB", "NLD");
        assertTrue(result);
    }

    @Test
    public void whenPutting123Dash4567AndJPNThenTrueIsExpected() {
        boolean result = PostCodeUtils.isCorrect("123-4567", "JPN");
        assertTrue(result);
    }

    @Test
    public void whenPuttingB9BSpace9B9AndCANThenTrueIsExpected() {
        boolean result = PostCodeUtils.isCorrect("B9B 9B9", "CAN");
        assertTrue(result);
    }
}