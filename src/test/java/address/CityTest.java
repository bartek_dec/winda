package address;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class CityTest {

    @Test
    public void whenCallingLUBLINThenItsStringRepresentationIsExpected() {
        String result = City.LUBLIN.getCityName();
        assertThat(result, is("Lublin"));
    }

    @Test
    public void whenGettingPopulationForLUBLINThen342000IsExpected() {
        int result = City.LUBLIN.getPopulation();
        assertEquals(342000, result);
    }

    @Test
    public void whenCallingToStringForWARSZAWAThenItsStringRepresentationIsExpected() {
        String result = City.WARSZAWA.toString();
        assertThat(result, is("Warszawa"));
    }

    @Test
    public void whenCallingGetInfoForLUBLINThenCityNameAndItsPopulationIsExpected() {
        String result = City.LUBLIN.getInfoAboutCity();
        assertThat(result, is("Lublin ma 342000 mieszkańców."));
    }

}